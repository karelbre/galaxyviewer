import QtQuick 2.2
import QtQuick.Controls 2.2

Text
{
	width: parent.width
	horizontalAlignment: Text.AlignHCenter
	font.bold: true
	font.underline: true
	font.pointSize: 12
}
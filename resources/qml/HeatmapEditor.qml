import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import QtQuick.Dialogs 1.0
import QtGraphicalEffects 1.0

Rectangle
{
	id: heatmapRoot
	height: parent.height
	width: parent.width
	color: "blue"

	property int durationAnims: 1000
	property int heightMainMenu
	property bool opened: false
	property bool menuActionTrigger: false
	property alias sourceImage: map.imagePath

	signal afterMenuAnim(bool opened)

	signal paint(int x, int y, int size, int red, int green, int blue, int alpha, bool isPreview)
	signal channelStateChanged(int channel, bool state)
	signal paintShapeChanged(int shapeType)
	signal discard()
	signal save()

	// Animated background
	LinearGradient
	{
		id: colorBg
		anchors.fill: parent
		start: Qt.point(0,0)
		end: Qt.point(0,0)

		property alias firstColor: firstColorBg.color
		property alias secondColor: secondColorBg.color
		property alias thirdColor: thirdColorBg.color

		gradient: Gradient
		{
			GradientStop
			{
				id: firstColorBg
				position: 0.0
				color: "#245900"
			}

			GradientStop
			{
				id: secondColorBg
				position: 0.5
				color: "#3b9300"
			}

			GradientStop
			{
				id: thirdColorBg
				position: 1.0
				color: "#245900"
			}
		}

		SequentialAnimation on firstColor
		{
			ColorAnimation
			{
				from: "#245900"
				to: "#3b9300"
				duration: 5000
			}

			ColorAnimation
			{
				from: "#3b9300"
				to: "#245900"
				duration: 5000
			}
			loops: Animation.Infinite
		}

		SequentialAnimation on secondColor
		{
			ColorAnimation
			{
				from: "#3b9300"
				to: "#4ec400"
				duration: 5000
			}

			ColorAnimation
			{
				from: "#4ec400"
				to: "#3b9300"
				duration: 5000
			}
			loops: Animation.Infinite
		}

		SequentialAnimation on thirdColor
		{
			ColorAnimation
			{
				from: "#245900"
				to: "#3b9300"
				duration: 5000
			}

			ColorAnimation
			{
				from: "#3b9300"
				to: "#245900"
				duration: 5000
			}
			loops: Animation.Infinite
		}
	}

	// Outer table
	Rectangle
	{
		id: mainMenuBackground
		anchors.fill: parent
		anchors.margins: 40
		
		// Background
		LinearGradient
		{
			id: innerBg
			anchors.fill: parent
			anchors.margins: 5

			start: Qt.point(0,0)
			end: Qt.point(0,0)

			gradient: Gradient
			{
				GradientStop
				{
					position: 0.0
					color: "#74d334"
				}

				GradientStop
				{
					id: debugCol
					position: 0.5
					color: "#5fad2b"
				}

				GradientStop
				{
					position: 1.0
					color: "#57992a"
				}
			}
		}

		// Border
		Rectangle
		{
			anchors.fill: parent
			color: "transparent"
			border.color: "#163063"
			border.width: 5
			radius: 30
		}
		
		// Inner table
		RowLayout
		{
			id: grid
			anchors.fill: parent
			anchors.margins: 50

			// Values bank
			Item
			{
				id: values
				property int paintSize: 1
			}

			// Tool table
			Rectangle
			{
				id: toolOptionsGrid
				anchors.top: parent.top
				anchors.left: parent.left
				anchors.bottom: parent.bottom
				width: prob_option.width + 20

				color: "#BDD0FC"
				border.color: activeFocus ? "black" : "gray"
				border.width: 2
				radius: 5
			
				ColumnLayout
				{
					id: toolOptions

					anchors.horizontalCenter: toolOptionsGrid.horizontalCenter
					spacing: 10

					// Header
					Label
					{
						id: toolOptionsLabel
						anchors.horizontalCenter: toolOptions.horizontalCenter
						text: "Tools"
						color: "white"
						font.pointSize: 15
						font.bold: true
						horizontalAlignment: Text.AlignHCenter
						style: Text.Outline
						styleColor: toolOptions.activeFocus ? "black" : "gray"
					}

					// Red channel
					SliderOption
					{
						id: prob_option
						anchors.horizontalCenter: toolOptions.horizontalCenter
						text: "Temperature"
						colorScheme: "red"
						checked: true

						onCheckedChanged:
						{
							channelStateChanged(0, checked)
							if (mouseArea)
							{
								mouseArea.refreshImage()
							}
						}
					}
					// Green channel
					SliderOption
					{
						id: temp_option
						anchors.horizontalCenter: toolOptions.horizontalCenter
						text: "Gen. probability"
						colorScheme: "green"
						checked: true

						onCheckedChanged:
						{
							channelStateChanged(1, checked)
							if (mouseArea)
							{
								mouseArea.refreshImage()
							}
						}
					}
					// Blue channel
					SliderOption
					{
						id: no_set_option1
						anchors.horizontalCenter: toolOptions.horizontalCenter
						text: "Not set (blue)"
						colorScheme: "blue"
						checked: true

						onCheckedChanged:
						{
							channelStateChanged(2, checked)
							if (mouseArea)
							{
								mouseArea.refreshImage()
							}
						}
					}
					// Alpha channel
					SliderOption
					{
						id: no_set_option2
						anchors.horizontalCenter: toolOptions.horizontalCenter
						text: "Not set (alpha)"
						colorScheme: "yellow"
						checked: true

						onCheckedChanged:
						{
							channelStateChanged(3, checked)
							if (mouseArea)
							{
								mouseArea.refreshImage()
							}
						}
					}
					// Size
					SliderOption
					{
						id: size_option
						anchors.horizontalCenter: toolOptions.horizontalCenter
						text: "Brush size"
						colorScheme: "transparent"
						checked: true
						checkBoxVisible: false
						value: 1
						from: 1
						to: 100

						onValueChanged:
						{
							values.paintSize = value
						}
					}
					// Shapes
					RowLayout
					{
						id: shapeType
						anchors.horizontalCenter: parent.horizontalCenter

						property int type: 0

						Rectangle
						{
							id: sphereCircle
							width: sphereImg.implicitWidth + 8
							height: sphereImg.implicitHeight + 8
							border.width: 4
							border.color: shapeType.type == typeId ? "red" : "transparent"

							property int typeId: 0

							Image
							{
								id: sphereImg
								anchors.horizontalCenter: parent.horizontalCenter
								anchors.verticalCenter: parent.verticalCenter
								width: 64
								height: 64
								source: "qrc:/textures/ShapeCircle.png"
							}

							MouseArea
							{
								anchors.fill: parent
								onClicked:
								{
									if (shapeType.type != sphereCircle.typeId)
									{
										shapeType.type = sphereCircle.typeId
										paintShapeChanged(sphereCircle.typeId)
									}
								}
							}
						}
				
						Rectangle
						{
							id: rectShape
							width: rectImg.implicitWidth + 8
							height: rectImg.implicitHeight + 8
							border.width: 4
							border.color: shapeType.type == typeId ? "red" : "transparent"

							property int typeId: 1
				
							Image
							{
								id: rectImg
								anchors.horizontalCenter: parent.horizontalCenter
								anchors.verticalCenter: parent.verticalCenter
								width: 64
								height: 64
								source: "qrc:/textures/ShapeRect.png"
							}

							MouseArea
							{
								anchors.fill: parent
								onClicked:
								{
									if (shapeType.type != rectShape.typeId)
									{
										shapeType.type = rectShape.typeId
										paintShapeChanged(rectShape.typeId)
									}
								}
							}
						}
					}
					// Buttons
					RowLayout
					{
						anchors.horizontalCenter: parent.horizontalCenter

						ButtonCustom
						{
							text: "Discard"
							pointSize: 12
							onClicked:
							{
								discard()
							}
						}

						ButtonCustom
						{
							text: "Save"
							pointSize: 12
							onClicked:
							{
								save()
							}
						}
					}
				}
			}

			// Heatmap image
			Item
			{
				anchors.top: parent.top
				anchors.left: toolOptionsGrid.right
				anchors.bottom: parent.bottom
				anchors.right: parent.right
				anchors.leftMargin: 20

				// Border
				Rectangle
				{
					id: flickableBorder

					anchors.fill: flickable
					anchors.margins: -5
					color: "#BDD0FC"
					border.color: toolOptionsGrid.activeFocus ? "black" : "gray"
					border.width: 5
					radius: 5
				}

				// Image place
				Flickable
				{
					id: flickable
					anchors.top: parent.top
					anchors.left: parent.left
					anchors.margins: 5
		
					property real minZoom: 
					{
						if (map.width < map.height)
						{
							return height / map.height
						}
						else
						{
							return width / map.width
						}
					}

					height: width
					width: 
					{
						if (parent.width > (parent.height))
						{
							return parent.height - (flickableBorder.border.width * 2)
						}
						else
						{
							return parent.width - (flickableBorder.border.width * 2)
						}
					}
					contentWidth: map.width*scaler.xScale
					contentHeight: map.height*scaler.xScale
					clip: true
					focus: true

					boundsBehavior: Flickable.StopAtBounds
				
					// Debug Area
					/*DebugRect { }*/
					Keys.onPressed:
					{
						if (event.key == Qt.Key_Q)
						{
							console.log(toolOptions.anchors.margins)
							console.log(toolOptions.anchors.topMargin)
							console.log(toolOptions.x)
							console.log(toolOptions.y)
							console.log(toolOptions.anchors.bottomMargin)
						}

						if (event.key == Qt.Key_R)
						{
							mouseArea.m_xActual = 0
							mouseArea.m_yActual = 0
							mouseArea.m_zoomActual = 1
							map.x = 0
							map.y = 0
						}
					}
					function printDebug()
					{
						console.log("**************************")
						console.log("Cursor position:          " + (cursorHelper.anchors.leftMargin+10) + " " + (cursorHelper.anchors.topMargin+10))
						console.log("Flickable position:       " + flickable.contentX + " " + flickable.contentY)
						console.log("Flickable test position:  " + flickable.mouseXPos + " " + flickable.mouseYPos)
						console.log("Scale image origin:       " + scaler.origin.x + " " + scaler.origin.y)
						console.log("Scale image:              " + scaler.xScale)
						console.log("**************************")
					}

					// Zooming and move
					MouseArea
					{
						id: mouseArea
						anchors.fill: parent

						acceptedButtons: Qt.LeftButton | Qt.RightButton
						hoverEnabled: true

						property bool paintTrigger: false
						property bool paintOn: false
						property int paintSize: values.paintSize

						property real m_xLast: 0
						property real m_yLast: 0
						property real m_xActual: 0
						property real m_yActual: 0
						property real m_zoomLast: 1.0
						property real m_zoomActual: 1.0
						property real m_zoomMax: 2
						property real m_zoomMin: flickable.minZoom

						onPressed:
						{
							if (mouse.button == Qt.RightButton)
							{
								mouseArea.paintOn = true
								flickable.enabled = false
								paint(mouseX / mouseArea.m_zoomLast, mouseY / mouseArea.m_zoomLast, 
									mouseArea.paintSize, prob_option.value, 
									temp_option.value, no_set_option1.value, no_set_option2.value, false)
								mouseArea.refreshImage()
							}
						}

						onPositionChanged:
						{
							paint(mouseX / mouseArea.m_zoomLast, mouseY / mouseArea.m_zoomLast, mouseArea.paintSize,
								prob_option.value, temp_option.value, no_set_option1.value, 
								no_set_option2.value, mouseArea.paintOn != true)
							mouseArea.refreshImage()
							mouse.accepted = true
						}

						onExited:
						{
							paint(mouseX / mouseArea.m_zoomLast, mouseY / mouseArea.m_zoomLast, 0,
								prob_option.value, temp_option.value, no_set_option1.value, 
								no_set_option2.value, mouseArea.paintOn != true)
							mouseArea.refreshImage()
						}

						onReleased:
						{
							if (mouse.button == Qt.RightButton)
							{
								mouseArea.paintOn = false
								mouseArea.focus = true
								flickable.enabled = true
							}
						}

						onWheel:
						{
							// Set new zoom
							var wheelDir = wheel.angleDelta.y > 0
							var newZoom = mouseArea.m_zoomLast + (wheelDir ? 0.1 : -0.1)
							mouseArea.m_zoomActual = wheelDir ? Math.min(newZoom, mouseArea.m_zoomMax) : Math.max(newZoom, mouseArea.m_zoomMin)
							var zoomDelta = mouseArea.m_zoomActual - mouseArea.m_zoomLast
							computeZoom(zoomDelta)
						}

						function zoomOutMax()
						{
							mouseArea.m_zoomActual = mouseArea.m_zoomMin
							var zoomDelta = mouseArea.m_zoomActual - mouseArea.m_zoomLast
							computeZoom(zoomDelta)
						}

						function refreshImage()
						{
							map.imagePath = mouseArea.paintTrigger ? "repeat2" : "repeat"
							mouseArea.paintTrigger = !mouseArea.paintTrigger
						}

						function computeZoom(zoomDelta)
						{
							// Save last values
							mouseArea.m_xLast = scaler.origin.x
							mouseArea.m_yLast = scaler.origin.y
							mouseArea.m_zoomLast = scaler.xScale

							// New origin of image
							mouseArea.m_xActual = mouseX / mouseArea.m_zoomLast
							mouseArea.m_yActual = mouseY / mouseArea.m_zoomLast
						
							// Set image offset position (correct)
							var deltaZoom = mouseArea.m_zoomActual - 1.0
							map.x = deltaZoom * mouseArea.m_xActual 
							map.y = deltaZoom * mouseArea.m_yActual 

							// Compute direction of flickable movement
							var dirX = (mouseArea.m_xActual / map.width)
							var dirY = (mouseArea.m_yActual / map.height)

							var subtractorX = zoomDelta * dirX * map.width
							var subtractorY = zoomDelta * dirY * map.height
						
							// Move flickable
							if (flickable.contentX < (flickable.contentWidth - flickable.width))
							{
								flickable.contentX = Math.max(0, Math.min(flickable.contentX + subtractorX, flickable.contentWidth - flickable.width))
							}

							if (flickable.contentY < (flickable.contentHeight - flickable.height))
							{
								flickable.contentY = Math.max(0, Math.min(flickable.contentY + subtractorY, flickable.contentHeight - flickable.height))
							}

							// Clamp flickable move
							flickable.contentX = Math.min(Math.max(flickable.contentX, 0), flickable.contentWidth - flickable.width)
							flickable.contentY = Math.min(Math.max(flickable.contentY, 0), flickable.contentHeight - flickable.height)
						}
					}

					// Heatmap
					Image
					{
						id: map

						property string imagePath: ""
						source: "image://heatmap/" + imagePath
						smooth: false
						cache: false

						transform: Scale
						{
							id: scaler
							origin.x: mouseArea.m_xActual
							origin.y: mouseArea.m_yActual
							xScale: mouseArea.m_zoomActual
							yScale: mouseArea.m_zoomActual
						}
					}
				}
			}
		}
	}
	
	// Title box
	Item
	{
		id: titleEditor
		anchors.top: parent.top
		anchors.horizontalCenter: parent.horizontalCenter
		implicitWidth: textApp.implicitWidth
		implicitHeight: textApp.implicitHeight
		anchors.topMargin: 10

		Rectangle 
		{
			anchors.fill: parent
			color: "#1049BA"
			border.color: "#163063"
			border.width: 5
			radius: 10
			anchors.bottomMargin: -5
			anchors.leftMargin: -10
			anchors.rightMargin: -10
		}

		Text
		{
			id: textApp
			text: "Heatmap editor"
			font.bold: true
			font.pointSize: 20
			color: "white"
		}

		Glow
		{
			anchors.fill: textApp
			radius: 5
			color: "black"
			source: textApp
		}
	}

	// Set to init state
	function init(height)
	{
		resize(height)
	}

	// Resize after window size has changed
	function resize(height)
	{
		if (!opened)
		{
			y = -height
		}

		heightMainMenu = height
		innerBg.end = Qt.point(0,height)
		colorBg.end = Qt.point(0,height)
		mouseArea.zoomOutMax()
	}

	// Open/Close menu
	function inverseState()
	{
		visible = true
		menuActionTrigger = true
		opened = !opened;
		mouseArea.zoomOutMax()
	}

	// Animations
	states: 
	[
		State
		{
			name: "Opened"
			when: opened
		
			PropertyChanges 
			{
				target: heatmapRoot
				y: 0
			}
		},
		
		State
		{
			name: "Closed"
			when: !opened
		
			PropertyChanges 
			{
				target: heatmapRoot
				y: -heightMainMenu
			}
		}
	]

	// Transitions between animations
	transitions: Transition
	{
		SequentialAnimation 
		{
			ParallelAnimation
			{
				NumberAnimation
				{
					properties: "y"
					duration: durationAnims
					easing.type: Easing.InOutQuad
				}

				ColorAnimation
				{
					duration: durationAnims
				}
			}

			ScriptAction
			{
				script:
				{
					if (heatmapRoot.menuActionTrigger)
					{
						heatmapRoot.menuActionTrigger = false
						heatmapRoot.afterMenuAnim(heatmapRoot.opened)
					}
				}
			}
		}
	}
}
import QtQuick 2.2
import QtQuick.Controls 2.2

GroupBox
{
	id: groupBox
	title: "Empty"

	label: Label
	{
		anchors.horizontalCenter: groupBox.horizontalCenter
		width: groupBox.availableWidth
		text: groupBox.title
		color: "white"
		font.pointSize: 15
		font.bold: true
		horizontalAlignment: Text.AlignHCenter
		style: Text.Outline
		styleColor: groupBox.activeFocus ? "black" : "gray"
	}

	background: Rectangle 
	{
        y: groupBox.topPadding - groupBox.padding
        width: groupBox.contentWidth + groupBox.leftPadding*2
        height: groupBox.height - groupBox.topPadding + groupBox.padding
        color: "#BDD0FC"
        border.color: groupBox.activeFocus ? "black" : "gray"
		border.width: 2
        radius: 5
    }
}
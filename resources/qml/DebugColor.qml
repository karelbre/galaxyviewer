import QtQuick 2.2

Item
{
	id: root
	anchors.fill: parent
	focus: true

	property bool enabledRect: false
	property color setColor

	signal colorChanged(color newColor)

	Rectangle
	{
		visible: root.enabledRect 
		anchors.fill : parent
		color: "red"
	}

	Keys.onPressed:
	{
		console.log("Barva je: " + setColor + " >> " + setColor.r + " " + setColor.g + " " + setColor.b)
		switch (event.key)
		{
			case Qt.Key_1:
				setColor.r -= 0.01 
				break;

			case Qt.Key_2:
				setColor.g -= 0.01 
				break;

			case Qt.Key_3:
				setColor.b -= 0.01 
				break;

			case Qt.Key_4:
				setColor.r += 0.01 
				break;

			case Qt.Key_5:
				setColor.g += 0.01 
				break;

			case Qt.Key_6:
				setColor.b += 0.01 
				break;
		}

		colorChanged(setColor)
	}
}
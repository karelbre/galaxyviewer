import QtQuick 2.9
import QtQuick.Controls 2.2
import GalaxyViewer.QmlMediatorCpp 1.0

ApplicationWindow
{
	id: root
	visible: true
	minimumWidth: 1024
    minimumHeight: 800
	width: 1024
    height: 800
	color: "black"

	// QML signals
	// Main menu signals
	signal generate()
	signal switchToMainMenu()
	signal saveSettings(string filepath)
	signal loadSettings(string filepath)
	signal loadDefaultSettings()
	// Heatmap editor signals
	signal saveImage()
	signal changePaintShape(int shape)
	signal changeChannelState(int channel, bool state)
	signal paintToImage(int x, int y, int size, int red, int green, int blue, int alpha, bool isPreview)
	// Space view signals
	signal changePauseState(bool state)
	signal saveGalaxy(string filepath)
	signal loadGalaxy(string filepath, bool start_render)

	// QML slots
	function onGalaxyGenerated()
	{
		switchMenu(1)
	}

	function onResize()
	{
		mainMenu.resize(height)
		heatmapEditor.resize(height)
		hud.resize(height)
	}

	// Escape triggered in space view
	function onTriggeredPauseMenu()
	{
		if (actualMenu == 1)
		{
			hud.triggerPauseMenu()
		}
	}

	// QML stuffs
	property int actualMenu: 0

	// First menu
	MainMenu
	{
		id: mainMenu
		visible: true
		shape_type: "Spiral"

		onEditHeatmap:
		{
			heatmapEditor.sourceImage = mainMenu.sourceImage
			root.switchMenu(2)
		}

		onLoadGalaxy: 
		{
			root.loadGalaxy(filepath, true)
			switchMenu(1)
		}

		onSaveSettings: root.saveSettings(filepath)
		onLoadSettings: root.loadSettings(filepath)
		onLoadDefaultSettings: root.loadDefaultSettings()
		onGenerate: root.generate()
	}

	// Screen for editing heatmap
	HeatmapEditor
	{
		id: heatmapEditor
		visible: false

		onAfterMenuAnim:
		{
			if (!opened)
			{
				visible = false
			}
		}

		onPaint: root.paintToImage(x, y, size, red, green, blue, alpha, isPreview)
		onChannelStateChanged: root.changeChannelState(channel, state)
		onPaintShapeChanged: root.changePaintShape(shapeType)

		onDiscard:
		{
			switchMenu(0)
		}

		onSave:
		{
			root.saveImage()
			mainMenu.reloadHeatmap()
			switchMenu(0)
		}
	}

	// Overlay hud in 3D
	SpaceView
	{
		id: hud
		visible: false

		onAfterMenuAnim:
		{
			if (!opened)
			{
				visible = false
			}
		}

		onSwitchMenu: 
		{
			root.switchToMainMenu()
			root.switchMenu(menuId)
		}

		onSaveGalaxy: root.saveGalaxy(filepath)
		onLoadGalaxy: root.loadGalaxy(filepath, false)
		onPauseStateChanged: root.changePauseState(state)
	}

	// Close actual menu and open menuId menu
	function switchMenu(menuId)
	{
		if (menuId == actualMenu)
		{
			return;
		}

		switch (actualMenu)
		{
			case 0:
				mainMenu.inverseState()
				break;

			case 1:
				hud.inverseState()
				break;

			case 2:
				heatmapEditor.inverseState()
				break;

			default:
				break;
		}

		switch (menuId)
		{
			case 0:
				mainMenu.inverseState()
				break;

			case 1:
				hud.inverseState()
				break;

			case 2:
				heatmapEditor.inverseState()
				break;

			default:
				break;
		}

		actualMenu = menuId
	}

	// Init after qml loaded fully
	Component.onCompleted:
	{
		mainMenu.init(height)
		heatmapEditor.init(height)
		hud.init(height)
	}
}
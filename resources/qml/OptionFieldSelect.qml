import QtQuick 2.2
import QtQuick.Controls 2.2

Rectangle
{
	id: field
	width: fieldText.width + fieldValue.width
	height: 20
	color: "transparent"

	property alias text: fieldText.text
	property alias value: fieldValue.currentIndex
	property alias model: fieldValue.model

	Text
	{
		id: fieldText
		text: "Empty"
		color: "black"
		font.pointSize: 9
		font.bold: true
		y: 5
		anchors.left: field.left
		anchors.leftMargin: 5
	}

	ComboBox
	{
		id: fieldValue
		anchors.left: fieldText.right
		anchors.leftMargin: 10
		width: implicitWidth + 30
		height: implicitHeight + 10

		contentItem: Text
		{
			color: fieldValue.activeFocus ? "black" : "gray"
			text: fieldValue.displayText
			font.pixelSize: 15
			verticalAlignment: Text.AlignVCenter
			horizontalAlignment: Text.AlignHCenter
		}

		background: Rectangle
		{
			id: fieldValueBg
			color: "white"
			border.color: fieldValue.pressed ? "black" : "gray"
			border.width: 1
			radius: 2
		}

		indicator: Item { }

		delegate: ItemDelegate
		{
			width: fieldValue.width
			height: fieldValue.height
			anchors.leftMargin: 5
			text: fieldValue.displayText
			highlighted: fieldValue.highlightedIndex == index
		}

		popup: Popup
		{
			id: fieldValuePopup
			y: parent.height
			width: parent.width
			height: parent.height*parent.count
			contentHeight: height
			contentWidth: width

			contentItem: ListView
			{
				id: popupList
				anchors.fill: parent
				model: fieldValue.model
				boundsBehavior: Flickable.StopAtBounds
				highlightFollowsCurrentItem: true
				focus: true

				highlight: Rectangle
				{
					color: "gray"
				}

				delegate: Item
				{
					width: popupList.width
					height: popupList.height/popupList.count

					Text
					{
						font.pointSize: 10
						text: modelData
						color: "#000000"
						anchors.horizontalCenter: parent.horizontalCenter
					}

					MouseArea 
					{
						id: mArea
						anchors.fill: parent
						onClicked: 
						{
							popupList.currentIndex = index
							fieldValue.currentIndex = index
							fieldValuePopup.close()
						}
					}
				}
			}

			background: Rectangle { color: "#FFFFFF" }
		}
	}
}
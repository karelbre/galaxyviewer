import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import QtQuick.Dialogs 1.0

Item
{
	id: spaceViewRoot
	anchors.fill: parent
	opacity: 0
	enabled: false
	focus: true

	property int durationAnims: 1000
	property bool opened: false
	property bool menuActionTrigger: false

	signal afterMenuAnim(bool opened)
	signal switchMenu(int menuId)
	signal pauseStateChanged(bool state)
	signal saveGalaxy(string filepath)
	signal loadGalaxy(string filepath)

	// Top panel
	Rectangle
	{
		id: topPanel
		color: "#66A0B5E5"
		border.color: "#d2ddf7"
		border.width: 2
		anchors.top: parent.top
		anchors.horizontalCenter: parent.horizontalCenter
		width: topPanelValues.implicitWidth + 20
		height: topPanelValues.implicitHeight + 35
		anchors.topMargin: -20
		radius: 20

		RowLayout
		{
			id: topPanelValues
			anchors.fill: parent
			anchors.horizontalCenter: parent.horizontalCenter
			anchors.verticalCenter: parent.verticalCenter
			anchors.topMargin: 10
			anchors.leftMargin: 10
			anchors.rightMargin: 10

			property bool heatmapGenerated: galaxy_info.generate_method == 1
			property bool isShapeCluster: galaxy_info.generate_shape == 2

			Layout.alignment: Qt.AlignVCenter | Qt.AlignHCenter
			spacing: 15

			TopValue
			{
				text: "Position: " + galaxy_info.camera_position
			}

			TopValue
			{
				text: "Suns: " + galaxy_info.suns_amount
			}

			TopValue
			{
				visible: topPanelValues.heatmapGenerated
				text: "Heatmap: " + galaxy_info.heatmap_filename
			}

			TopValue
			{
				visible: !topPanelValues.heatmapGenerated
				text: "Shape: " + parent.getShapeName(galaxy_info.generate_shape) 
			}

			TopValue
			{
				visible: !topPanelValues.heatmapGenerated && topPanelValues.isShapeCluster
				text: "Base: " + parent.getShapeName(cluster_data.basis)
			}

			TopValue
			{
				visible: !topPanelValues.heatmapGenerated && topPanelValues.isShapeCluster
				text: "Base amount: " + galaxy_info.basis_amount
			}

			function getShapeName(shape_type)
			{
				switch(shape_type)
				{
					case 0:
						return "Sphere";
					case 1:
						return "Grid";
					case 2:
						return "Cluster";
					case 3:
						return "Spiral";
					default:
						return "Not set";
				}
			}
		}
	}

	// Menu button
	Rectangle
	{
		id: menuButton
		color: menuButtonMouse.pressed ? "#5E1C1C33" : "#993333333"
		border.color: "lightgray"
		border.width: 3
		radius: 5
		anchors.left: parent.left
		anchors.top: parent.top
		anchors.leftMargin: 10
		anchors.topMargin: 10
		width: menuButtonText.implicitWidth + 20
		height: menuButtonText.implicitHeight + 10

		Text
		{
			id: menuButtonText
			anchors.fill: parent
			text: "MENU"
			color: "lightgray"
			font.pointSize: 15
			horizontalAlignment: Text.AlignHCenter
			verticalAlignment: Text.AlignVCenter
		}

		MouseArea
		{
			id: menuButtonMouse
			anchors.fill: parent
			onClicked:
			{
				menuPanel.invertState()
			}
		}
	}

	// Menu
	Rectangle
	{
		id: menuPanel
		opacity: 0
		color: "#66A0B5E5"
		border.color: "#d2ddf7E5"
		border.width: 3
		radius: 10
		width: menuPanelButtons.implicitWidth + 30
		height: menuPanelButtons.implicitHeight + 50
		anchors.horizontalCenter: parent.horizontalCenter
		anchors.verticalCenter: parent.verticalCenter

		property bool opened: false

		// Buttons
		ColumnLayout
		{
			id: menuPanelButtons
			enabled: parent.opened
			anchors.fill: parent
			anchors.margins: 10
			anchors.topMargin: 30
			anchors.bottomMargin: 30
			spacing: 10

			ButtonCustom
			{
				text: "Main menu"
				anchors.horizontalCenter: parent.horizontalCenter
				onClicked:
				{
					spaceViewRoot.switchMenu(0)
					menuPanel.opened = false
				}
			}

			ButtonCustom
			{
				text: "Save"
				anchors.horizontalCenter: parent.horizontalCenter
				onClicked: saveGalaxyDialog.open()
			}

			// Save galaxy dialog
			FileDialog
			{
				id: saveGalaxyDialog
				title: "Please choose a file"
				nameFilters: [ "Settings files (*.json)", "All files (*)" ]
				folder: shortcuts.home
				selectExisting: false

				onAccepted: spaceViewRoot.saveGalaxy(fileUrl)
			}

			ButtonCustom
			{
				text: "Load"
				anchors.horizontalCenter: parent.horizontalCenter
				onClicked: loadGalaxyDialog.open()
			}

			// Load galaxy dialog
			FileDialog
			{
				id: loadGalaxyDialog
				title: "Please choose a file"
				nameFilters: [ "Settings files (*.json)", "All files (*)" ]
				folder: shortcuts.home

				onAccepted: spaceViewRoot.loadGalaxy(fileUrl)
			}

			ButtonCustom
			{
				anchors.horizontalCenter: parent.horizontalCenter
				text: "Back"
				onClicked:
				{
					menuPanel.invertState()
				}
			}
		}

		function invertState()
		{
			opened = !opened
			spaceViewRoot.pauseStateChanged(opened)
		}

		states:
		[
			State
			{
				name: "Opened"
				when: menuPanel.opened

				PropertyChanges
				{
					target: menuPanel
					opacity: 1
				}
			},

			State
			{
				name: "Closed"
				when: !menuPanel.opened

				PropertyChanges
				{
					target: menuPanel
					opacity: 0
				}
			}
		]

		transitions: 
		[
			Transition
			{
				NumberAnimation
				{
					property: "opacity"
					duration: 100
				}
			}
		]
	}

	// React on escape key
	function triggerPauseMenu()
	{
		menuPanel.invertState()
	}

	// Open/Close menu
	function inverseState()
	{
		if (opened)
		{
			enabled = false	
		}

		visible = true
		menuActionTrigger = true
		opened = !opened;
	}

	function init(height)
	{ }

	function resize(height)
	{ }

	// Animations
	states: 
	[
		State
		{
			name: "Opened"
			when: spaceViewRoot.opened
		
			PropertyChanges 
			{
				target: spaceViewRoot
				opacity: 1
			}
		},
		
		State
		{
			name: "Closed"
			when: !spaceViewRoot.opened
		
			PropertyChanges 
			{
				target: spaceViewRoot
				opacity: 0
			}
		}
	]

	transitions: Transition
	{
		SequentialAnimation
		{
			NumberAnimation
			{
				property: "opacity"
				duration: durationAnims
				easing.type: Easing.InOutQuad
			}

			ScriptAction
			{
				script:
				{
					spaceViewRoot.enabled = spaceViewRoot.opened
					if (spaceViewRoot.menuActionTrigger)
					{
						spaceViewRoot.menuActionTrigger = false;
						spaceViewRoot.afterMenuAnim(spaceViewRoot.opened)
					}
				}
			}
		}
	}
}
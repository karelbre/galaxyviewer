import QtQuick 2.2

// Debug space
Rectangle
{
	anchors.fill: parent
	border.color: "red"
	border.width: 2

	signal clicked()

	property bool mouseEnabled: false

	MouseArea
	{
		anchors.fill: parent
		hoverEnabled: true
		enabled: mouseEnabled

		onClicked:
		{
			parent.clicked()
		}
	}
}
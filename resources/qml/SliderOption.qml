import QtQuick 2.2
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3

Rectangle
{
	id: root

	property bool checkBoxVisible: true

	width: slider.width
	height: (checkBoxVisible ? checkBox.height : buttonText.height) + slider.height
	color: Qt.lighter(colorScheme, 1.5)
	radius: 7

	property color colorScheme: "white"
	property alias text: checkBox.text
	property alias checked: checkBox.checked
	property alias value: slider.value
	property alias from: slider.from
	property alias to: slider.to

	ColumnLayout
	{
		anchors.fill: parent
		anchors.horizontalCenter: parent.horizontalCenter
		spacing: 0

		CheckBoxCustom
		{
			id: checkBox
			visible: root.checkBoxVisible
		}

		Text
		{
			id: buttonText
			color: "black"
			text: checkBox.text
			font.pointSize: 12
			font.bold: true
			anchors.horizontalCenter: parent.horizontalCenter
			visible: !root.checkBoxVisible
		}
		
		Slider 
		{
			id: slider
			from: 0
			to: 255
			value: 255

			background: Rectangle 
			{
				x: slider.leftPadding
				y: slider.topPadding + slider.availableHeight / 2 - height / 2
				implicitWidth: 200
				implicitHeight: 10
				width: slider.availableWidth
				height: implicitHeight
				radius: 4
				color: "gray"
				border.color: "black"
				border.width: 4

				Rectangle 
				{
					width: slider.visualPosition * parent.width
					height: parent.height
					color: root.colorScheme
					radius: 4
					border.color: "black"
					border.width: 2
				}
			}

			handle: Rectangle 
			{
				x: slider.leftPadding + slider.visualPosition * (slider.availableWidth - width)
				y: slider.topPadding + slider.availableHeight / 2 - height / 2
				implicitWidth: 26
				implicitHeight: 26
				radius: 13
				color: slider.pressed ? "#d1d1d1" : "#f6f6f6"
				border.color: "#969a9e"
			}
		}
	}
}
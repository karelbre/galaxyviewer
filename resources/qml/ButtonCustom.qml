import QtQuick 2.2
import QtQuick.Controls 2.2

Rectangle
{
	id: button
	width: buttonText.implicitWidth + extraWidth
	height: buttonText.implicitHeight + extraHeight
	color: 
	{
		if (enabled)
		{
			if (mouse.pressed)
			{
				return Qt.darker(colorEnabled, 0.7)
			}
			else
			{
				return entered ? colorEnabled : Qt.darker(colorEnabled, 1.3)
			}
		}
		else
		{
			return colorDisabled
		}
	}
	border.color: "black"
	border.width: 2
	radius: 10

	property int extraWidth: 40
	property int extraHeight: 15
	property string colorEnabled: "#CECECE"
	property string colorDisabled: "#66FFFFFF"

	property bool entered: false
	property bool enabled: true

	property alias text: buttonText.text
	property alias pointSize: buttonText.font.pointSize

	signal clicked()

	Text
	{
		id: buttonText
		text: "Empty"
		anchors.fill: button
		horizontalAlignment: Text.AlignHCenter
		verticalAlignment: Text.AlignVCenter
		font.pointSize: 10
		font.bold: true
	}

	MouseArea
	{
		id: mouse
		anchors.fill: parent
		hoverEnabled: true

		onClicked: 
		{
			if (button.enabled)
			{
				button.clicked()
			}
		}

		onEntered:
		{
			button.entered = true
		}

		onExited:
		{
			button.entered = false
		}
	}
}
import QtQuick 2.2
import QtQuick.Controls 2.2

Rectangle
{
	id: field
	width: parent.width
	height: 20
	color: "transparent"

	property real childWidth: (width - fieldText.leftMargin - 
		fieldText.width - fieldValueX.anchors.leftMargin - 
		fieldValueZ.anchors.rightMargin) / 3.0

	property alias text: fieldText.text
	property bool isReal: true
	property int floatingNumbers: 3
	property alias valueX: fieldValueX.text
	property alias valueY: fieldValueY.text
	property alias valueZ: fieldValueZ.text

	Text
	{
		id: fieldText
		text: "Empty"
		color: "black"
		font.pointSize: 10
		font.bold: true
		y: 5
		anchors.left: field.left
		anchors.leftMargin: 10
	}

	TextField
	{
		id: fieldValueX
		anchors.left: fieldText.right
		anchors.leftMargin: 20
		horizontalAlignment: TextInput.AlignHCenter
		width: field.childWidth
		selectByMouse: true

		background: Rectangle
		{
			width: fieldValueX.width + fieldValueX.leftPadding
			color: "white"
			border.color: "black"
			border.width: 1
		}

		MouseArea 
		{
			anchors.fill: parent
			cursorShape: Qt.IBeamCursor
			acceptedButtons: Qt.NoButton
		}
	}

	TextField
	{
		id: fieldValueY
		anchors.left: fieldValueX.right
		anchors.leftMargin: 10
		horizontalAlignment: TextInput.AlignHCenter
		width: field.childWidth
		selectByMouse: true

		background: Rectangle
		{
			width: fieldValueY.width + fieldValueY.leftPadding
			color: "white"
			border.color: "black"
			border.width: 1
		}

		MouseArea 
		{
			anchors.fill: parent
			cursorShape: Qt.IBeamCursor
			acceptedButtons: Qt.NoButton
		}
	}

	TextField
	{
		id: fieldValueZ
		anchors.left: fieldValueY.right
		anchors.right: field.right
		anchors.leftMargin: 10
		anchors.rightMargin: 30
		horizontalAlignment: TextInput.AlignHCenter
		width: field.childWidth
		selectByMouse: true

		background: Rectangle
		{
			width: fieldValueZ.width + fieldValueZ.leftPadding
			color: "white"
			border.color: "black"
			border.width: 1
		}

		MouseArea 
		{
			anchors.fill: parent
			cursorShape: Qt.IBeamCursor
			acceptedButtons: Qt.NoButton
		}
	}
}
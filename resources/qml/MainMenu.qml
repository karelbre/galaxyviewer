import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.Controls.Styles 1.4
import QtQuick.Layouts 1.3
import QtQuick.Dialogs 1.0
import QtGraphicalEffects 1.0

// Main Menu screen
Rectangle
{
	id: mainMenuRootObject
	height: parent.height
	width: parent.width

	property int durationAnims: 1000
	property int heightMainMenu
	property bool opened: false
	property bool menuActionTrigger: false
	property string shape_type
	property string sourceImage
	property bool refreshImageFlag: false

	signal loadGalaxy(string filepath)
	signal saveSettings(string filepath)
	signal loadSettings(string filepath)
	signal loadDefaultSettings()
	signal afterMenuAnim(bool opened)
	signal generate()
	signal editHeatmap()

	// Animated background
	LinearGradient
	{
		id: colorBg
		anchors.fill: parent
		start: Qt.point(0,0)
		end: Qt.point(0,0)

		property alias firstColor: firstColorBg.color
		property alias secondColor: secondColorBg.color
		property alias thirdColor: thirdColorBg.color

		gradient: Gradient
		{
			GradientStop
			{
				id: firstColorBg
				position: 0.0
				color: "#000000"
			}

			GradientStop
			{
				id: secondColorBg
				position: 0.5
				color: "#493aa3"
			}

			GradientStop
			{
				id: thirdColorBg
				position: 1.0
				color: "#000000"
			}
		}

		SequentialAnimation on firstColor
		{
			ColorAnimation
			{
				from: "#000000"
				to: "#0e2f59"
				duration: 5000
			}

			ColorAnimation
			{
				from: "#0e2f59"
				to: "#000000"
				duration: 5000
			}
			loops: Animation.Infinite
		}

		SequentialAnimation on secondColor
		{
			ColorAnimation
			{
				from: "#0e2f59"
				to: "#6454c4"
				duration: 5000
			}

			ColorAnimation
			{
				from: "#6454c4"
				to: "#0e2f59"
				duration: 5000
			}
			loops: Animation.Infinite
		}

		SequentialAnimation on thirdColor
		{
			ColorAnimation
			{
				from: "#000000"
				to: "#0e2f59"
				duration: 5000
			}

			ColorAnimation
			{
				from: "#0e2f59"
				to: "#000000"
				duration: 5000
			}
			loops: Animation.Infinite
		}
	}

	// Outer table
	Rectangle
	{
		id: mainMenuBackground
		anchors.fill: parent
		anchors.margins: 40
		 
		LinearGradient
		{
			id: innerBg
			anchors.fill: parent
			anchors.margins: 5

			start: Qt.point(0,0)
			end: Qt.point(0,0)

			gradient: Gradient
			{
				GradientStop
				{
					position: 0.0
					color: "#7788C7"
				}

				GradientStop
				{
					id: debugCol
					position: 0.5
					color: "#6561C9"
				}

				GradientStop
				{
					position: 1.0
					color: "#3264C9"
				}
			}
		}

		Rectangle
		{
			anchors.fill: parent
			color: "transparent"
			border.color: "#163063"
			border.width: 5
			radius: 30
		}

		// Inner content
		RowLayout
		{
			id: grid
			anchors.fill: parent
			anchors.margins: 50
			
			// Left 
			ColumnLayout
			{
				id: leftGrid
				anchors.top: grid.top
				anchors.left: grid.left
				spacing: 30
				// Methods selector
				GroupBoxCustom
				{
					id: methodSelector
					title: "Methods"

					property string heatmapFile: ""

					// Radio button listener
					ButtonGroup
					{
						buttons: generatorMethodsButtons.children
						onClicked: 
						{
							var isSelectedShape = button.idButton == 0
							galaxy_info.generate_method = button.idButton
							shapeSelector.visible = (isSelectedShape)

							if (isSelectedShape)
							{
								methodSelector.heatmapFile = heatmapPreviewImage.imagePath
								heatmapPreviewImage.isHeatmapImage = false
								shapeSelector.reloadShapePreview(-2, "")
								previewImageLabel.text = "Shape preview"
							}
							else
							{
								heatmapPreviewImage.imagePath = methodSelector.heatmapFile
								heatmapPreviewImage.isHeatmapImage = true
								previewImageLabel.text = "Heatmap preview"
							}

							rightGrid.onSelectedMethod(button.idButton)
							middleGrid.switchOptions(isSelectedShape ? galaxy_info.generate_shape : 4)
						}
					}

					// Option's hub
					Column
					{
						id: generatorMethodsButtons

						RadioButtonCustom
						{
							idButton: 0
							text: "Shapes"
							checked: true
						}

						RadioButtonCustom
						{
							idButton: 1
							text: "Heatmap"
						}
					}
				}

				// Shape selector
				GroupBoxCustom
				{
					id: shapeSelector
					title: "Types"
					visible: true
					property int lastIdButton: -1
					property string lastButtonText: "Sphere"
					property string lastClusterBasisText: "Sphere"

					// Radio button listener
					ButtonGroup
					{
						buttons: shapeTypesButtons.children
						onClicked: 
						{
							galaxy_info.generate_shape = button.idButton
							shapeSelector.reloadShapePreview(button.idButton, button.text)
							middleGrid.switchOptions(button.idButton)
						}
					}

					// Option's hub
					Column
					{
						id: shapeTypesButtons

						RadioButtonCustom
						{
							idButton: 0
							text: "Sphere"
							checked: true
						}

						RadioButtonCustom
						{
							idButton: 1
							text: "Grid"
						}

						RadioButtonCustom
						{
							idButton: 2
							text: "Cluster"
						}

						RadioButtonCustom
						{
							idButton: 3
							text: "Spiral"
						}
					}

					function reloadShapePreview(idButton, buttonText)
					{
						// Set shape type text
						if ((idButton >= 0) && (idButton <= 3))
						{
							lastIdButton = idButton
							lastButtonText = buttonText
						}

						var append = ""
						// If shape is cluster
						if (lastIdButton == 2)
						{
							// Set name of cluster basis
							if (idButton == -1)
							{
								lastClusterBasisText = buttonText
							}
							append = lastClusterBasisText
						}

						heatmapPreviewImage.imagePath = "res:///" + lastButtonText + append + ".png"
					}
				}
			}

			// Middle
			ColumnLayout
			{
				id: middleGrid
				anchors.left: leftGrid.right
				anchors.right: rightGrid.left
				anchors.top: grid.top
				anchors.bottom: grid.bottom
				anchors.leftMargin: 50
				anchors.rightMargin: 50

				property int actualOptionsId: 0

				function switchOptions(id)
				{
					switch (actualOptionsId)
					{
						case 0:
							sphereOptions.visible = false
							break;
						case 1:
							gridOptions.visible = false
							break;
						case 2:
							clusterOptions.visible = false
							break;
						case 3:
							spiralOptions.visible = false
							break;
						case 4:
							heatmapOptions.visible = false
							break;
						default:
							return;
					}

					switch (id)
					{
						case 0:
							optionsSelector.contentChildren = sphereOptions
							sphereOptions.visible = true
							break;
						case 1:
							optionsSelector.contentChildren = gridOptions
							gridOptions.visible = true
							break;
						case 2:
							optionsSelector.contentChildren = clusterOptions
							clusterOptions.visible = true
							break;
						case 3:
							optionsSelector.contentChildren = spiralOptions
							spiralOptions.visible = true
							break;
						case 4:
							optionsSelector.contentChildren = heatmapOptions
							heatmapOptions.visible = true
							break;
						default:
							return;
					}

					actualOptionsId = id
				}

				Label
				{
					id: optionsLabel
					anchors.top: middleGrid.top
					anchors.horizontalCenter: middleGrid.horizontalCenter
					text: "Options"
					color: "white"
					font.pointSize: 15
					font.bold: true
					horizontalAlignment: Text.AlignHCenter
					style: Text.Outline
					styleColor: optionsSelector.activeFocus ? "black" : "gray"
				}

				// Scrollview border
				Rectangle
				{
					anchors.fill: parent
					anchors.top: optionsLabel.bottom
					anchors.bottom: optionsButtons.top
					anchors.topMargin: 40
					anchors.bottomMargin: optionsButtons.implicitHeight + 10
					color: "#BDD0FC"
					border.color: optionsSelector.activeFocus ? "black" : "gray"
					border.width: 2
				}

				// Content
				ScrollView
				{
					id: optionsSelector
					anchors.fill: parent
					anchors.top: optionsLabel.bottom
					anchors.bottom: optionsButtons.top
					anchors.margins: 3
					anchors.topMargin: 43
					anchors.bottomMargin: optionsButtons.implicitHeight + 13
					clip: true
					
					ScrollBar.horizontal.policy: ScrollBar.AsNeeded
					ScrollBar.vertical: ScrollBar
					{
						parent: optionsSelector
						x: optionsSelector.width - width
						y: optionsSelector.topPadding
						height: optionsSelector.availableHeight
						policy: ScrollBar.AlwaysOn
						width: 20
						active: true
					}

					background: Rectangle
					{
						color: "#BDD0FC"
					}
				}

				// Options buttons
				RowLayout
				{
					id: optionsButtons
					anchors.bottom: middleGrid.bottom
					anchors.horizontalCenter: middleGrid.horizontalCenter
					spacing: 10

					// Save settings
					ButtonCustom
					{
						text: "Save"
						pointSize: 12
						onClicked: saveSettingsDialog.open()
					}

					// Load settings
					ButtonCustom
					{
						text: "Load"
						pointSize: 12
						onClicked: loadSettingsDialog.open()
					}

					// Reset defaults
					ButtonCustom
					{
						text: "Reset"
						pointSize: 12
						onClicked:
						{
							mainMenuRootObject.loadDefaultSettings();
						}
					}

					// Save settings dialog
					FileDialog
					{
						id: saveSettingsDialog
						title: "Please choose a file"
						nameFilters: [ "Settings files (*.galaxysettings)", "All files (*)" ]
						folder: shortcuts.home
						selectExisting: false

						onAccepted: mainMenuRootObject.saveSettings(fileUrl)
					}

					// Load settings dialog
					FileDialog
					{
						id: loadSettingsDialog
						title: "Please choose a file"
						nameFilters: [ "Settings files (*.galaxysettings)", "All files (*)" ]
						folder: shortcuts.home

						onAccepted: mainMenuRootObject.loadSettings(fileUrl)
					}
				}

				// Options container
				Item
				{
					id: optionsContainer
					visible: false
					
					// Sphere options
					Column
					{
						id: sphereOptions
						width: optionsSelector.width
						spacing: 10
						visible: true

						OptionField
						{
							text: "Size"
							value: sphere_data.size

							onValueChanged:
							{
								sphere_data.size = value
							}
						}

						OptionField
						{
							text: "Density deviation"
							value: sphere_data.density_deviation

							onValueChanged:
							{
								sphere_data.density_deviation = value
							}
						}

						OptionField
						{
							text: "Density mean"
							value: sphere_data.density_mean

							onValueChanged:
							{
								sphere_data.density_mean = value
							}
						}

						OptionFieldVec3
						{
							text: "Deviation (x,y,z)"
							valueX: sphere_data.deviation_x
							valueY: sphere_data.deviation_y
							valueZ: sphere_data.deviation_z

							onValueXChanged:
							{
								sphere_data.deviation_x = valueX
							}

							onValueYChanged:
							{
								sphere_data.deviation_y = valueY
							}

							onValueZChanged:
							{
								sphere_data.deviation_z = valueZ
							}
						}

						Component.onCompleted:
						{
							optionsSelector.contentChildren = sphereOptions
						}
					}

					// Grid options
					Column
					{
						id: gridOptions
						width: optionsSelector.width
						spacing: 10
						visible: false

						OptionField
						{
							text: "Size"
							value: grid_data.size

							onValueChanged:
							{
								grid_data.size = value
							}
						}

						OptionField
						{
							text: "Spacing"
							value: grid_data.spacing

							onValueChanged:
							{
								grid_data.spacing = value
							}
						}
					}

					// Cluster options
					Column
					{
						id: clusterOptions
						width: optionsSelector.width
						spacing: 10
						visible: false

						OptionFieldSelect
						{
							text: "Base shape"
							model: ["Sphere", "Grid", "Spiral"]
							value: cluster_data.basis

							onValueChanged:
							{
								cluster_data.basis = value
								shapeSelector.reloadShapePreview(-1, model[value])
							}
						}

						OptionField
						{
							text: "Count deviation"
							value: cluster_data.count_deviation

							onValueChanged:
							{
								cluster_data.count_deviation = value
							}
						}

						OptionField
						{
							text: "Count mean"
							value: cluster_data.count_mean

							onValueChanged:
							{
								cluster_data.count_mean = value
							}
						}

						OptionFieldVec3
						{
							text: "Deviation (x,y,z)"
							valueX: cluster_data.deviation_x
							valueY: cluster_data.deviation_y
							valueZ: cluster_data.deviation_z

							onValueXChanged:
							{
								cluster_data.deviation_x = valueX
							}

							onValueYChanged:
							{
								cluster_data.deviation_y = valueY
							}

							onValueZChanged:
							{
								cluster_data.deviation_z = valueZ
							}
						}
					}

					// Spiral options
					Column
					{
						id: spiralOptions
						width: optionsSelector.width
						spacing: 10
						visible: false

						OptionField
						{
							text: "Size"
							value: spiral_data.size

							onValueChanged:
							{
								spiral_data.size = value
							}
						}

						OptionField
						{
							text: "Spacing"
							value: spiral_data.spacing

							onValueChanged:
							{
								spiral_data.spacing = value
							}
						}

						OptionField
						{
							text: "Minimum arms"
							value: spiral_data.minimum_arms
							isReal: false

							onValueChanged:
							{
								spiral_data.minimum_arms = value
							}
						}

						OptionField
						{
							text: "Maximum arms"
							value: spiral_data.maximum_arms
							isReal: false

							onValueChanged:
							{
								spiral_data.maximum_arms = value
							}
						}

						OptionsLabel
						{
							text: "Cluster"
						}

						OptionField
						{
							text: "Count deviation"
							value: spiral_data.cluster_count_deviation

							onValueChanged:
							{
								spiral_data.cluster_count_deviation = value
							}
						}

						OptionField
						{
							text: "Center deviation"
							value: spiral_data.cluster_center_deviation

							onValueChanged:
							{
								spiral_data.cluster_center_deviation = value
							}
						}

						OptionField
						{
							text: "Min arm scale"
							value: spiral_data.min_arm_cluster_scale

							onValueChanged:
							{
								spiral_data.min_arm_cluster_scale = value
							}
						}

						OptionField
						{
							text: "Max arm scale"
							value: spiral_data.max_arm_cluster_scale

							onValueChanged:
							{
								spiral_data.max_arm_cluster_scale = value
							}
						}

						OptionField
						{
							text: "Arm scale deviation"
							value: spiral_data.arm_cluster_scale_deviation

							onValueChanged:
							{
								spiral_data.arm_cluster_scale_deviation = value
							}
						}
						
						OptionField
						{
							text: "Swirl"
							value: spiral_data.swirl

							onValueChanged:
							{
								spiral_data.swirl = value
							}
						}

						OptionField
						{
							text: "Center scale"
							value: spiral_data.center_cluster_scale

							onValueChanged:
							{
								spiral_data.center_cluster_scale = value
							}
						}

						OptionField
						{
							text: "Center density mean"
							value: spiral_data.center_cluster_density_mean

							onValueChanged:
							{
								spiral_data.center_cluster_density_mean = value
							}
						}

						OptionField
						{
							text: "Center density deviation"
							value: spiral_data.center_cluster_density_deviation

							onValueChanged:
							{
								spiral_data.center_cluster_density_deviation = value
							}
						}

						OptionField
						{
							text: "Center size deviation"
							value: spiral_data.center_cluster_size_deviation

							onValueChanged:
							{
								spiral_data.center_cluster_size_deviation = value
							}
						}

						OptionField
						{
							text: "Center position deviation"
							value: spiral_data.center_cluster_position_deviation

							onValueChanged:
							{
								spiral_data.center_cluster_position_deviation = value
							}
						}

						OptionField
						{
							text: "Center count deviation"
							value: spiral_data.center_cluster_count_deviation

							onValueChanged:
							{
								spiral_data.center_cluster_count_deviation = value
							}
						}

						OptionField
						{
							text: "Center count mean"
							value: spiral_data.center_cluster_count_mean

							onValueChanged:
							{
								spiral_data.center_cluster_count_mean = value
							}
						}

						OptionField
						{
							text: "Central void size mean"
							value: spiral_data.central_void_size_mean

							onValueChanged:
							{
								spiral_data.central_void_size_mean = value
							}
						}

						OptionField
						{
							text: "Central void size deviation"
							value: spiral_data.central_void_size_deviation

							onValueChanged:
							{
								spiral_data.central_void_size_deviation = value
							}
						}

						Rectangle
						{
							color: "transparent"
							width: parent.width
							height: 20
						}
					}

					// Heatmap options
					Column
					{
						id: heatmapOptions
						width: optionsSelector.width
						spacing: 10
						visible: true

						OptionFieldVec3
						{
							text: "Size (x,y,z)"
							valueX: heatmap_data.size_x
							valueY: heatmap_data.size_y
							valueZ: heatmap_data.size_z

							onValueXChanged:
							{
								heatmap_data.size_x = valueX
							}

							onValueYChanged:
							{
								heatmap_data.size_y = valueY
							}

							onValueZChanged:
							{
								heatmap_data.size_z = valueZ
							}
						}

						OptionField
						{
							text: "Height deviation"
							value: heatmap_data.height_deviation

							onValueChanged:
							{
								heatmap_data.height_deviation = value
							}
						}

						OptionField
						{
							text: "Minimal distance"
							value: heatmap_data.minimal_distance

							onValueChanged:
							{
								heatmap_data.minimal_distance = value
							}
						}

						OptionField
						{
							text: "Temperature min"
							value: heatmap_data.temp_range_min

							onValueChanged:
							{
								heatmap_data.temp_range_min = value
							}
						}

						OptionField
						{
							text: "Temperature max"
							value: heatmap_data.temp_range_max

							onValueChanged:
							{
								heatmap_data.temp_range_max = value
							}
						}

						OptionField
						{
							text: "Temperature deviation"
							value: heatmap_data.temp_deviation

							onValueChanged:
							{
								heatmap_data.temp_deviation = value
							}
						}

						Component.onCompleted:
						{
							optionsSelector.contentChildren = heatmapOptions
						}
					}
				}
			}

			// Right
			ColumnLayout
			{
				id: rightGrid
				anchors.top: grid.top
				anchors.right: grid.right
				anchors.bottom: grid.bottom

				function onSelectedMethod(idMethod)
				{
					buttonsHM.setButtonsState(idMethod == 1)
					buttonsApp.setButtonsState(idMethod == 1)
				}

				Label
				{
					id: previewImageLabel
					anchors.horizontalCenter: rightGrid.horizontalCenter
					text: "Shape preview"
					color: "white"
					font.pointSize: 15
					font.bold: true
					horizontalAlignment: Text.AlignHCenter
					style: Text.Outline
					styleColor: heatmapPreviewImage.activeFocus ? "black" : "gray"
				}

				// Heatmap preview
				Rectangle
				{
					color: "lightgray"
					border.color: "black"
					border.width: 5
					width: 300
					height: 300 

					Image
					{
						id: heatmapPreviewImage
						anchors.fill: parent
						anchors.margins: parent.border.width
						smooth: false 
						cache: false
						property string imagePath: "res:///Circle.png"
						property bool isHeatmapImage: false

						source: "image://preview/" + imagePath
					}
				}

				// Heatmap Buttons
				RowLayout
				{
					id: buttonsHM
					anchors.horizontalCenter: rightGrid.horizontalCenter

					// Load heatmap
					ButtonCustom
					{
						id: loadButtonHM
						text: "Load"
						pointSize: 12
						enabled: false

						onClicked:
						{
							loadHeatmap.open()
						}
					}

					// Load heatmap dialog
					FileDialog
					{
						id: loadHeatmap
						title: "Please choose a file"
						nameFilters: [ "Image files (*.png)", "All files (*)" ]
						folder: shortcuts.home

						onAccepted:
						{
							mainMenuRootObject.sourceImage = fileUrl
							heatmapPreviewImage.imagePath = fileUrl;
							heatmapPreviewImage.isHeatmapImage = true
							buttonsHM.setButtonsState(true)
							buttonsApp.setButtonsState(true)
						}
					}

					ButtonCustom
					{
						id: editButtonHM
						text: "Edit"
						pointSize: 12
						enabled: false

						onClicked:
						{
							mainMenuRootObject.editHeatmap()
						}
					}

					// Enable buttons for heatmap
					function setButtonsState(state)
					{
						if (state)
						{
							loadButtonHM.enabled = true
							editButtonHM.enabled = heatmapPreviewImage.isHeatmapImage && (heatmapPreviewImage.imagePath != "")
						}
						else
						{
							loadButtonHM.enabled = false
							editButtonHM.enabled = false
						}
					}
				}

				// App buttons
				Column
				{
					id: buttonsApp
					anchors.bottom: rightGrid.bottom
					anchors.right: rightGrid.right
					anchors.left: rightGrid.left
					spacing: 10

					ButtonCustom
					{
						id: buttonGen
						text: "Generate!"
						pointSize: 15
						anchors.right: parent.right
						anchors.left: parent.left
						colorEnabled: "#14b200"
						colorDisabled: "#cc0000"

						onClicked:
						{
							buttonGen.forceActiveFocus()
							mainMenuRootObject.generate()
						}
					}

					ButtonCustom
					{
						text: "Load Galaxy"
						pointSize: 15
						anchors.right: parent.right
						anchors.left: parent.left
						colorEnabled: "#FFB600"
						colorDisabled: "#FFB60066"

						onClicked: loadGalaxyDialog.open()
					}

					// Load galaxy dialog
					FileDialog
					{
						id: loadGalaxyDialog
						title: "Please choose a file"
						nameFilters: [ "Settings files (*.json)", "All files (*)" ]
						folder: shortcuts.home

						onAccepted: mainMenuRootObject.loadGalaxy(fileUrl)
					}

					// Set butons interactable flag
					function setButtonsState(state)
					{
						if (state)
						{
							buttonGen.enabled = heatmapPreviewImage.isHeatmapImage && (heatmapPreviewImage.imagePath != "")
						}
						else
						{
							buttonGen.enabled = true
						}
					}
				}
			}
		}
	}

	// Title box
	Item
	{
		id: titleApp
		anchors.top: parent.top
		anchors.horizontalCenter: parent.horizontalCenter
		implicitWidth: textApp.implicitWidth
		implicitHeight: textApp.implicitHeight
		anchors.topMargin: 10

		Rectangle 
		{
			anchors.fill: parent
			color: "#1049BA"
			border.color: "#163063"
			border.width: 5
			radius: 10
			anchors.bottomMargin: -5
			anchors.leftMargin: -10
			anchors.rightMargin: -10
		}

		Text
		{
			id: textApp
			text: "Galaxy Viewer"
			font.bold: true
			font.pointSize: 20
			color: "white"
		}

		Glow
		{
			anchors.fill: textApp
			radius: 5
			color: "black"
			source: textApp
		}
	}

	// Set to init state
	function init(height)
	{
		resize(height)
		opened = true
	}

	// Resize after window size has changed
	function resize(height)
	{
		if (!opened)
		{
			y = height
		}

		heightMainMenu = height
		colorBg.end = Qt.point(0,height)
		innerBg.end = Qt.point(0,height)
	}

	// Reload preview image of heatmap
	function reloadHeatmap()
	{
		heatmapPreviewImage.imagePath = refreshImageFlag ? "repeat1" : "repeat2"
		refreshImageFlag = !refreshImageFlag
	}

	// Open/Close menu
	function inverseState()
	{
		visible = true
		menuActionTrigger = true
		opened = !opened;
	}

	// Animations
	states: 
	[
		State
		{
			name: "Opened"
			when: opened
		
			PropertyChanges 
			{
				target: mainMenuRootObject
				y: 0
			}
		},
		
		State
		{
			name: "Closed"
			when: !opened
		
			PropertyChanges 
			{
				target: mainMenuRootObject
				y: heightMainMenu
			}
		}
	]

	// Transitions between animations
	transitions: Transition
	{
		SequentialAnimation 
		{
			ParallelAnimation
			{
				NumberAnimation
				{
					properties: "y"
					duration: durationAnims
					easing.type: Easing.InOutQuad
				}

				ColorAnimation
				{
					duration: durationAnims
				}
			}

			ScriptAction
			{
				script:
				{
					if (mainMenuRootObject.menuActionTrigger)
					{
						mainMenuRootObject.menuActionTrigger = false
						mainMenuRootObject.afterMenuAnim(mainMenuRootObject.opened)
					}
				}
			}
		}
	}
}
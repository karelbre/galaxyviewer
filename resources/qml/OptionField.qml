import QtQuick 2.2
import QtQuick.Controls 2.2

Rectangle
{
	id: field
	width: parent.width
	height: 20
	color: "transparent"

	property bool isReal: true
	property int floatingNumbers: 3
	property alias text: fieldText.text
	property alias value: fieldValue.text

	Text
	{
		id: fieldText
		text: "Empty"
		color: "black"
		font.pointSize: 10
		font.bold: true
		y: 5
		anchors.left: field.left
		anchors.leftMargin: 10
	}

	TextField
	{
		id: fieldValue
		anchors.right: field.right
		anchors.left: fieldText.right
		anchors.leftMargin: 20
		anchors.rightMargin: 30
		horizontalAlignment: TextInput.AlignHCenter
		selectByMouse: true

		onTextChanged:
		{
			// todo: udelat zkraceni textu a kontrolu na nepovolene znaky
			//text = 
		}

		background: Rectangle
		{
			width: fieldValue.width + fieldValue.leftPadding
			color: "white"
			border.color: "black"
			border.width: 1
		}

		MouseArea 
		{
			anchors.fill: parent
			cursorShape: Qt.IBeamCursor
			acceptedButtons: Qt.NoButton
		}
	}
}
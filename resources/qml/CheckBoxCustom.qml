import QtQuick 2.2
import QtQuick.Controls 2.2

CheckBox 
{
	id: button
	implicitWidth: indicator.implicitWidth + buttonTextItem.implicitWidth

	property int idButton

	indicator: Rectangle 
	{
		id: indicator
		implicitWidth: 32
		implicitHeight: 32
		radius: 7
		color: "#C9D5ED"
		border.color: button.activeFocus ? "black" : "gray"
		border.width: 5
			
		Rectangle 
		{
			anchors.fill: parent
			visible: button.checked
			color: button.activeFocus ? "red" : "gray"
			radius: 5
			anchors.margins: 8
		}
	}

	contentItem: Item
	{
		id: buttonTextItem
		anchors.left: indicator.right
		anchors.leftMargin: 10
		implicitWidth: buttonText.implicitWidth
		implicitHeight: buttonText.implicitHeight
		baselineOffset: buttonText.y + buttonText.baselineOffset

		Text
		{
			id: buttonText
			color: "black"
			text: button.text
			font.pointSize: 12
			font.bold: true
		}
	}
}
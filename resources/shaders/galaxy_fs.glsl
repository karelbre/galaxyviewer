#version 430

in float tempOut;
out vec4 fragColor;
uniform vec2 temp_range;
uniform float debug_render;

/**
 * Convert sun set temperature number to color
 */
vec4 convertTempToColor(float temp)
{
	temp -= temp_range.x;
	vec4 rgba;
	rgba.r = temp / temp_range.y;
	rgba.g = 0;
	rgba.b = 1 - rgba.r;
	rgba.a = 1;

	return rgba;
}

void main()
{
	fragColor = debug_render > 0.5f ? vec4(0f, 0f, 0f, 1f) : convertTempToColor(tempOut);
}
#version 430

layout(location = 0) in vec3 position;

struct SunSet
{
	vec3 pos;
	float temp;
};

layout(std430, binding = 0) buffer sunSetsBuffer
{
	SunSet sun_sets[];
};

uniform mat4 v; // view
uniform mat4 p; // projection

out float tempOut;

void main()
{
	SunSet sun_set = sun_sets[gl_InstanceID];
	gl_Position = p * v * vec4(position + sun_set.pos, 1);
	tempOut = sun_set.temp;
}
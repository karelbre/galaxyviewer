======================
Subject: Master thesis
======================
Name: GalaxyViewer
======================
Last updated: 29.4.2018
======================
Author: Karel Březina
======================
Description: 

Application shows possibilities of GalaxyGen library.
CMake (version 3.8.0 or higher) is needed for create build solution.
Dependencies => Qt5
             => GPUEngine library
             => GLM library
             => GalaxyGen library

Features
=> 

Controls in 3D mode 
=> W - forward move
=> A - left move
=> S - backward move
=> D - right move
=> C - down move
=> Space - up move
=> M - switch between normal/debug render mode
=> B - make sun model bigger (no limitations)
=> N - make sun model smaller (to minimal size)
=> R - reset camera position to galaxy center (0,0,0)
=> / - raise camera movement speed
=> * - lower camera movement speed
=> + - raise camera rotation sensitivity
=> - - lower camera rotation sensitivity

Git repository: https://bitbucket.org/karelbre/galaxyviewer
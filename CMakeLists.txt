# Minimalni verze CMAKE
cmake_minimum_required(VERSION 3.8.0)

# Prom nazev projektu
set(APP_NAME GalaxyViewer)
set(SRC_DIR src)
set(INCLUDE_DIR include)

# Nastaveni nazvu projektu
project(${APP_NAME})

# Prom nastavitelne z CMAKE
set(CMAKE_INCLUDE_CURRENT_DIR ON)
set(CMAKE_AUTORCC ON)
set(CMAKE_AUTOUIC ON)
set(CMAKE_AUTOMOC ON)

# Napojim slozku s knihovnou
find_package(GPUEngine COMPONENTS geGL geUtil)
find_package(GalaxyGen)
find_package(Qt5 COMPONENTS Quick)

# Prom seznam zdrojovych souboru
set(APP_SOURCES 
    ${SRC_DIR}/AppController.cpp
    ${SRC_DIR}/GalaxyRenderer.cpp
    ${SRC_DIR}/HeatmapImageProvider.cpp
    ${SRC_DIR}/HeatmapPreviewProvider.cpp
    ${SRC_DIR}/InputController.cpp
    ${SRC_DIR}/main.cpp
    ${SRC_DIR}/QmlMediatorCpp.cpp
    ${SRC_DIR}/QuickRendererBase.cpp
    ${SRC_DIR}/ShapePainters.cpp
)

# Prom seznam hlavickovych souboru
include_directories(${INCLUDE_DIR})
set(APP_INCLUDES 
    ${INCLUDE_DIR}/AppController.h
    ${INCLUDE_DIR}/GalaxyRenderer.h
    ${INCLUDE_DIR}/HeatmapImageProvider.h
    ${INCLUDE_DIR}/HeatmapPreviewProvider.h
    ${INCLUDE_DIR}/InputController.h
    ${INCLUDE_DIR}/QmlMediatorCpp.h
    ${INCLUDE_DIR}/QuickRendererBase.h
    ${INCLUDE_DIR}/ShapePainters.h
)

# Nastavim target 
add_executable(${APP_NAME} ${APP_SOURCES} ${APP_INCLUDES} resources/qml.qrc resources/galaxyviewer.rc)
target_link_libraries(${APP_NAME} geGL geUtil GalaxyGen Qt5::Quick)

set(DEFAULT_RESOURCES_PATH "${CMAKE_INSTALL_PREFIX}/resources")
set(${APP_NAME}_RESOURCES "${DEFAULT_RESOURCES_PATH}" CACHE PATH "Relative or absolute path to Application resources.")
set(IS_STANDALONE CACHE BOOL "If you want to use application as standalone, check-in this flag for empty resource path.")

if (IS_STANDALONE)
    set_target_properties(${APP_NAME} PROPERTIES COMPILE_DEFINITIONS "${APP_NAME}_RESOURCES=\"resources/\"")
else()
    set_target_properties(${APP_NAME} PROPERTIES COMPILE_DEFINITIONS "${APP_NAME}_RESOURCES=\"${${APP_NAME}_RESOURCES}/\"")
endif()


install(TARGETS ${APP_NAME}
    RUNTIME DESTINATION ./
)

# Release libraries
install(
    FILES
        ${geGL_DIR}/../../bin/geCore.dll
        ${geGL_DIR}/../../bin/geGL.dll
        ${geGL_DIR}/../../bin/geUtil.dll
        ${GalaxyGen_DIR}/../../../bin/GalaxyGen.dll
    CONFIGURATIONS
        Release
    DESTINATION
        ./
)

install(
    DIRECTORY
        ${GalaxyGen_DIR}/../../../resources
    DESTINATION
        ./
)

install(
    DIRECTORY
        resources
    DESTINATION
        ./
)

install(
    CODE
        "execute_process(COMMAND \"${Qt5_DIR}/../../../bin/windeployqt.exe\" \"--qmldir\" \"${CMAKE_INSTALL_PREFIX}/resources/qml\" \"${CMAKE_INSTALL_PREFIX}\")"
)

include(InstallRequiredSystemLibraries)
install(
    FILES 
        ${CMAKE_INSTALL_SYSTEM_RUNTIME_LIBS} 
    DESTINATION 
        ./
    COMPONENT 
        Libraries
)

message(STATUS "${APP_NAME} configured successfully!")
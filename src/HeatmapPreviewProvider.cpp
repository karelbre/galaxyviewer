#include <HeatmapPreviewProvider.h>

#include <QDebug>

#include <glm/vec4.hpp>
#include <QDir>

namespace galaxy_viewer
{
	/**
	 * Constructor
	 */
	HeatmapPreviewProvider::HeatmapPreviewProvider() : QQuickImageProvider(Image)
	{ }

	/**
	 * Get image (also for updating image)
	 * @param id filepath or reload instruction
	 */
	QImage HeatmapPreviewProvider::requestImage(const QString& id, QSize*, const QSize&)
	{
		QString filename = id;
		if (id.startsWith("file:///"))
		{
			filename = id.mid(8);
		}

		if (id.startsWith("res:///"))
		{
			QDir dir(QString(GalaxyViewer_RESOURCES) + "textures");
			filename = dir.absoluteFilePath(id.mid(7));
		}

		// No image to load
		if (filename.count() == 0)
		{
			filename_ = "";
			QImage empty = QPixmap(1, 1).toImage();
			return empty;
		}

		// Update loaded image
		if (filename.startsWith("repeat"))
		{
			filename = filename_;
		}

		filename_ = filename;
		QImage image;
		// Load new image
		if (!image.load(filename_) || (image.format() != QImage::Format_ARGB32))
		{
			qDebug() << "Image loading failed. Wrong file path or missing any channel from RGBA?";
			filename_ = "";
			QImage empty = QPixmap(1, 1).toImage();
			return empty;
		}

		heatmap_ = image;
		return image;
	}

	/**
	 * Copy heatmap pixels to data structure
	 * @return heatmap texture pixels
	 */
	glm::vec4** HeatmapPreviewProvider::GetPixels() const
	{
		QSize size = heatmap_.size();
		const int height = size.height();
		const int width = size.width();

		glm::vec4** pixels = new glm::vec4*[height];
		for (int y = 0; y < height; ++y)
		{
			glm::vec4* row = new glm::vec4[width];
			for (int x = 0; x < width; ++x)
			{
				QColor color = heatmap_.pixelColor(x, y);
				row[x] = glm::vec4(color.redF(), color.greenF(), color.blueF(), color.alphaF());
			}
			pixels[y] = row;
		}

		return pixels;
	}

	/**
	 * Get heatmap size
	 * @return Size
	 */
	QSize HeatmapPreviewProvider::GetSize() const
	{
		return heatmap_.size();
	}

	/**
	 * Get heatmap filename
	 * @return Filename
	 */
	QString HeatmapPreviewProvider::GetFilename() const
	{
		return filename_.right(filename_.length() - filename_.lastIndexOf("/") - 1);
	}

	/**
	 * Check if image was successfully loaded
	 */
	bool HeatmapPreviewProvider::IsImageLoaded() const
	{
		return filename_ != "";
	}
}

#include <HeatmapImageProvider.h>

#include <QDebug>

namespace galaxy_viewer
{
	/**
	 * Constructor
	 */
	HeatmapImageProvider::HeatmapImageProvider() : QQuickImageProvider(Image)
	{
		painter_ = std::make_shared<PainterCircle>();
		painter_type_ = CIRCLE;
		was_last_preview_ = false;
		paint_width_preview_ = 0.0f;
	}

	/**
	 * Get image (also for updating image)
	 * @param id filepath or reload instruction
	 */
	QImage HeatmapImageProvider::requestImage(const QString& id, QSize*, const QSize&)
	{
		QString filename = id;
		if (id.startsWith("file:///"))
		{
			filename = id.mid(8);
		}

		// No image to load
		if (filename.count() == 0)
		{
			filename_ = "";
			QImage empty = QPixmap(1, 1).toImage();
			return empty;
		}

		// Update loaded image
		if (filename.startsWith("repeat"))
		{
			return heatmap_modified_;
		}

		filename_ = filename;
		// Load new image
		if (!heatmap_modified_.load(filename_, "") || (heatmap_modified_.format() != QImage::Format_ARGB32))
		{
			qDebug() << "Image loading failed. Wrong file path or missing any channel from RGBA?";
			filename_ = "";
			QImage empty = QPixmap(1, 1).toImage();
			return empty;
		}

		heatmap_all_channels_.load(filename_);
		return heatmap_modified_;
	}

	/**
	 * Set heatmap texture channel to on/off
	 * @param channel selected color channel
	 * @param state channel visibile/invisible
	 */
	void HeatmapImageProvider::SetChannel(PainterBase::channel_type channel, bool state)
	{
		for (int y = 0; y < heatmap_modified_.height(); ++y)
		{
			for (int x = 0; x < heatmap_modified_.width(); ++x)
			{
				QColor col = heatmap_modified_.pixelColor(x, y);
				switch (channel)
				{
				case PainterBase::RED:
					col.setRed(state ? heatmap_all_channels_.pixelColor(x, y).red() : 0);
					break;

				case PainterBase::GREEN:
					col.setGreen(state ? heatmap_all_channels_.pixelColor(x, y).green() : 0);
					break;

				case PainterBase::BLUE:
					col.setBlue(state ? heatmap_all_channels_.pixelColor(x, y).blue() : 0);
					break;

				case PainterBase::ALPHA:
					col.setAlpha(state ? heatmap_all_channels_.pixelColor(x, y).alpha() : 0);
					break;
				}

				heatmap_modified_.setPixelColor(x, y, col);
			}
		}

		channel_state_[channel] = state;
	}

	/**
	 * Paint brush pattern to texture
	 * @param x brush x position
	 * @param y brush y position
	 * @param size brush size
	 * @param color brush color
	 * @param is_preview is painting temporary or permanent
	 */
	void HeatmapImageProvider::SetColor(int x, int y, int size, QColor color, bool is_preview)
	{
		const int arm = (size - 1) / 2;
		const int paint_width = (arm * 2);

		if (is_preview)
		{
			if (was_last_preview_)
			{
				painter_->RemovePreview(heatmap_all_channels_, heatmap_modified_, paint_pos_preview_, paint_width_preview_, channel_state_);
			}

			paint_pos_preview_.x = x;
			paint_pos_preview_.y = y;
			paint_width_preview_ = paint_width;
			was_last_preview_ = true;

			painter_->PaintPreview(heatmap_modified_, paint_pos_preview_, paint_width, color, channel_state_);
		}
		else
		{
			painter_->Paint(heatmap_all_channels_, heatmap_modified_, glm::vec2(x, y), paint_width, color, channel_state_);
		}

	}

	/*
	 * Change brush pattern
	 * @param type brush pattern (0 = Circle, 1 = Rect)
	 */
	void HeatmapImageProvider::ChangePainter(shape_type type)
	{
		if (type == painter_type_)
		{
			return;
		}

		painter_type_ = type;
		switch (type)
		{
			case RECTANGLE:
				painter_ = std::make_shared<PainterRect>();
				break;

			case CIRCLE:
				painter_ = std::make_shared<PainterCircle>();
				break;
		}
	}

	/*
	 * Save heatmap to file
	 */
	void HeatmapImageProvider::SaveAndReset()
	{
		heatmap_all_channels_.save(filename_);

		for (int i = 0; i < 4; ++i)
		{
			channel_state_[i] = true;
		}

		filename_ = "";
		was_last_preview_ = false;
	}

	/**
	 * Check if image was successfully loaded
	 */
	bool HeatmapImageProvider::IsImageLoaded() const
	{
		return filename_ != "";
	}
}
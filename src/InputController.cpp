#include <InputController.h>

namespace galaxy_viewer
{
	/**
	 * Constructor
	 */
	InputController::InputController()
	{
		waiting_for_cursor_centered_ = false;
		is_inited_ = false;
	}

	/**
	 * Initialize component. Need to call it before anything else!
	 * @param window_center center point of app window
	 */
	void InputController::Init(QPoint window_center, float cursor_range)
	{
		center_ = window_center;
		distance_ = center_ * cursor_range;
		last_mouse_pos = center_;
		is_inited_ = true;
	}

	/**
	 * Signal handler of component
	 * @param watched handling component 
	 * @param event received event
	 */
	bool InputController::eventFilter(QObject* watched, QEvent* event)
	{
		switch(event->type())
		{
			case QEvent::KeyPress:
			{
				QKeyEvent* keyEvent = static_cast<QKeyEvent*>(event);
				if (keyEvent->isAutoRepeat())
				{
					return true;
				}

				emit OnKeyPressed(keyEvent->key());
				break;
			}

			case QEvent::KeyRelease:
			{
				QKeyEvent* keyEvent = static_cast<QKeyEvent*>(event);
				if (keyEvent->isAutoRepeat())
				{
					return true;
				}

				emit OnKeyReleased(keyEvent->key());
				break;
			}

			case QEvent::MouseMove:
			{
				if (!is_inited_)
				{
					break;
				}

				QMouseEvent* mouseEvent = static_cast<QMouseEvent*>(event);
				QPoint pos = mouseEvent->pos();

				if (waiting_for_cursor_centered_)
				{
					if (IsCursorNearToCenter(pos, centered_range_))
					{
						last_mouse_pos = pos;
					}
				}

				const int dx = pos.x() - last_mouse_pos.x();
				const int dy = pos.y() - last_mouse_pos.y();
				last_mouse_pos = pos;
				
				emit OnMouseMove(dx, dy);

				if (!waiting_for_cursor_centered_ && !IsCursorNearToCenter(pos, distance_))
				{
					CursorNeedToBeCentered();
					waiting_for_cursor_centered_ = true;
				}

				break;
			}

			case QEvent::Leave:
			{
				emit OnMouseLeave();
				break;
			}

			case QEvent::Enter:
			{
				emit OnMouseEnter();
				break;
			}

			case QEvent::FocusIn:
			{
				emit OnFocusIn();
				break;
			}

			case QEvent::Resize:
			{
				emit OnResize();
				break;
			}

			default:
				break;
		}

		return QObject::eventFilter(watched, event);
	}

	/**
	 * Detect if cursor passed set range
	 * @param cursor_pos current cursor position
	 * @param range cursor permitted area distance from window center
	 */
	bool InputController::IsCursorNearToCenter(QPoint cursor_pos, QPoint range) const
	{
		return 
		((abs(cursor_pos.x() - center_.x()) <= range.x()) &&
		 (abs(cursor_pos.y() - center_.y()) <= range.y()));
	}
}

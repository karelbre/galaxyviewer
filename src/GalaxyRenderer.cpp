#include <GalaxyRenderer.h>

#include <QOpenGLContext>
#include <QDebug>
#include <QDir>
#include <geCore/Text.h>
#include <geGL/geGL.h>
#include <geUtil/FreeLookCamera.h>

// OGL Debug messages
//#undef GLDEBUGPROC
//#define typedef void(*GLDEBUGPROC)(GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length, const GLchar *message, const void *userParam);

namespace galaxy_viewer
{
	/**
	 * Structure for instance indirect rendering
	 */
	struct DrawArraysIndirectCommand
	{
		GLuint count;
		GLuint instanceCount;
		GLuint first;
		GLuint baseInstance;
	};

	/**
	 * Constructor
	 */
	GalaxyRenderer::GalaxyRenderer(QObject* parent) : QuickRendererBase(parent), gl_(nullptr), shaderProgram_(nullptr)
	{
		is_paused_ = true;
		is_draw_enabled_ = false;
		is_initialized_ = false;

		rotation_smooth_factor_ = 0.5f;
		rotation_delta_ = glm::vec2(0, 0);

		need_to_reload_ = false;
		sphere_vertices_ = 0;
	}

	/**
	 * Pause renderer updating and reset movement
	 */
	void GalaxyRenderer::SetPause(bool state)
	{
		is_paused_ = state;
		ResetMovement();
	}

	/**
	 * Update scene. Camera movement, rotation.
	 */
	void GalaxyRenderer::Update()
	{
		if (need_to_reload_)
		{
			// Reload data to shaders
			DrawArraysIndirectCommand params =
			{
				static_cast<GLuint>(sphere_vertices_), static_cast<GLuint>(sun_list_vec_->size()), static_cast<GLuint>(0), static_cast<GLuint>(0)
			};

			sun_list_buffer_ = std::make_shared<ge::gl::Buffer>(gl_->getFunctionTable(), sizeof(galaxy_generator::SunListData)*sun_list_vec_->size(), sun_list_vec_->data());
			indirect_buffer_ = std::make_shared<ge::gl::Buffer>(gl_->getFunctionTable(), sizeof(DrawArraysIndirectCommand), &params);

			need_to_reload_ = false;
			is_draw_enabled_ = true;
		}

		if (reload_sun_model_)
		{
			LoadSunModel();
			reload_sun_model_ = false;
		}

		UpdateCameraRotation();
		UpdateCameraMove();
	}

// Movement
#if 1 
	/**
	 * Set movement values to default 
	 */
	void GalaxyRenderer::ResetMovement()
	{
		move_state_[RIGHT] = false;
		move_state_[LEFT] = false;
		move_state_[UP] = false;
		move_state_[DOWN] = false;
		move_state_[FORWARD] = false;
		move_state_[BACK] = false;
	}

	/**
	 * Set camera position to default
	 */
	void GalaxyRenderer::ResetCameraPosition()
	{
		free_look_camera_.setPosition(glm::vec3(0, 0, 0));
		emit CameraPositionChanged(0, 0, 0);
	}
	
	/**
	 * Set movement value
	 * @param dir movement dir
	 * @param state is moving?
	 */
	void GalaxyRenderer::SetMoving(move_dir dir, bool state)
	{
		move_state_[dir] = state;
	}

	/**
	 * Set camera rotation
	 * @param dx camera delta x position
	 * @param dy camera delta y position
	 */
	void GalaxyRenderer::SetRotation(int dx, int dy)
	{
		rotation_delta_.x += dy;
		rotation_delta_.y += dx;
	}

	/**
	 * Set sensitivity type value 
	 * @param type rotation or movement
	 * @param is_adding increasing value?
	 * @param value changing value 
	 */
	void GalaxyRenderer::SetSensitivity(sensitivity_type type, bool is_adding, float value)
	{
		sensitivity_[type] = std::max(0.0f, sensitivity_[type] + sensitivity_step_vals_[type] * (is_adding ? value : -value));
	}

	/**
	 * Update smooth camera rotation. Call from update loop
	 */
	void GalaxyRenderer::UpdateCameraRotation()
	{
		// For smooth rotation 
		const float dx = rotation_delta_.x * rotation_smooth_factor_;
		const float dy = rotation_delta_.y * rotation_smooth_factor_;

		free_look_camera_.addXAngle(dx * sensitivity_[ROTATION]);
		free_look_camera_.addYAngle(dy * sensitivity_[ROTATION]);

		rotation_delta_.x -= dx;
		rotation_delta_.y -= dy;
	}

	/**
	 * Update camera position. Call from update loop
	 */
	void GalaxyRenderer::UpdateCameraMove()
	{
		const glm::ivec3 last_position = free_look_camera_.getPosition();
		const float move_value = camera_move * (move_state_[SHIFT] ? acceleration_mul : 1) * sensitivity_[MOVEMENT];

		if (move_state_[RIGHT])
		{
			free_look_camera_.right(move_value);
		}
		if (move_state_[LEFT])
		{
			free_look_camera_.left(move_value);
		}
		if (move_state_[FORWARD])
		{
			free_look_camera_.forward(move_value);
		}
		if (move_state_[BACK])
		{
			free_look_camera_.back(move_value);
		}
		if (move_state_[UP])
		{
			free_look_camera_.up(move_value);
		}
		if (move_state_[DOWN])
		{
			free_look_camera_.down(move_value);
		}

		const glm::ivec3 position = free_look_camera_.getPosition();
		if (position != last_position)
		{
			emit CameraPositionChanged(position.x, position.y, position.z);
		}
	}
	
#endif

// Callbacks
#if 1
	/**
	 * Called when QQuickWindow is resized
	 */
	void GalaxyRenderer::OnResize()
	{
		QSize size = qqw_->size();
		perspective_camera_.setAspect(static_cast<float>(size.width()) / size.height());
	}

	/**
	 * This is called with active ogl context so we prep the scene and draw it.
	 * The body of the rendering loop is here.
	 */
	void GalaxyRenderer::OnAfterRendering()
	{

	}

	/**
	 * This is called with active ogl context so we prep the scene and draw it.
	 * The body of the rendering loop is here.
	 */
	void GalaxyRenderer::OnBeforeRendering()
	{
		if (is_paused_)
		{
			return;
		}

		Update();
		SetupGlState();
		Draw();
		qqw_->resetOpenGLState();
	}

	/**
	 * This function is called when the context is created. Here you should
	 * do the general init of things that have the application wide lifetime.
	 */
	void GalaxyRenderer::OnOGLContextCreated(QOpenGLContext* context)
	{
		context->makeCurrent(qqw_);

		// Init GL
		ge::gl::init();
		gl_ = std::make_shared<ge::gl::Context>();

		// Shaders
		const std::string shaders_dir(GalaxyViewer_RESOURCES + std::string("shaders/"));
		auto simple_vs = std::make_shared<ge::gl::Shader>(GL_VERTEX_SHADER, ge::core::loadTextFile(shaders_dir + "galaxy_vs.glsl"));
		auto simple_fs = std::make_shared<ge::gl::Shader>(GL_FRAGMENT_SHADER, ge::core::loadTextFile(shaders_dir + "galaxy_fs.glsl"));

		shaderProgram_ = std::make_shared<ge::gl::Program>(simple_vs, simple_fs);

		// Prepare VAO, VBO
		vao_ = std::make_shared<ge::gl::VertexArray>();
		LoadSunModel();
		vao_->addAttrib(vbo_, 0, 3, GL_FLOAT);

		// Set camera
		perspective_camera_.setAspect(static_cast<float>(qqw_->width()) / qqw_->height());
		perspective_camera_.setFar(5000.0f);
		perspective_camera_.setFovy(45);

		// OGL debug output
		//gl_->glEnable(GL_DEBUG_OUTPUT);
		//setDefaultDebugMessage(gl_);

		is_initialized_ = true;

		context->doneCurrent();
	}
#endif

// Suns data management
#if 1
	/**
	 * Mark new render data to reload
	 * @param sun_list data to render
	 * @param temp_range range of computed temperature
	 */
	void GalaxyRenderer::SetSunListData(std::shared_ptr<std::vector<galaxy_generator::SunListData>> sun_list, 
		glm::vec2 temp_range, glm::vec3 camera_position, glm::vec3 camera_rotation)
	{
		sun_list_vec_ = sun_list;
		temp_range_ = temp_range;
		need_to_reload_ = true;
		is_draw_enabled_ = false;
		free_look_camera_.setPosition(camera_position);
		free_look_camera_.setXAngle(camera_rotation.x);
		free_look_camera_.setYAngle(camera_rotation.y);
		free_look_camera_.setZAngle(camera_rotation.z);
		emit CameraPositionChanged(camera_position.x, camera_position.y, camera_position.z);
	}

	/**
	 * Setup common basic OGL state for your vizualization. Beware, the
	 * Qt has its own setup due to gui rendering. For example blending
	 * function may be set differently.
	 */
	void GalaxyRenderer::SetupGlState() const
	{
		if (render_mode_debug)
		{
			gl_->glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
		}
		else
		{
			gl_->glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
		}

		gl_->glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		gl_->glBindTexture(GL_TEXTURE_2D, 0);
		gl_->glEnable(GL_DEPTH_TEST);
		gl_->glViewport(0, 0, qqw_->width(), qqw_->height());
	}

	/**
	 * Draw set sun list
	 */
	void GalaxyRenderer::Draw()
	{
		if (!is_draw_enabled_)
		{
			return;
		}

		glm::mat4 view = free_look_camera_.getView();
		glm::mat4 projection = perspective_camera_.getProjection();

		shaderProgram_->set1f("debug_render", render_mode_debug ? 1 : 0);
		shaderProgram_->set2f("temp_range", temp_range_.x, temp_range_.y);
		shaderProgram_->setMatrix4fv("v", glm::value_ptr(view));
		shaderProgram_->setMatrix4fv("p", glm::value_ptr(projection));
		shaderProgram_->use();

		vao_->bind();
		indirect_buffer_->bind(GL_DRAW_INDIRECT_BUFFER);
		sun_list_buffer_->bindBase(GL_SHADER_STORAGE_BUFFER, 0);
		gl_->glMultiDrawArraysIndirect(GL_TRIANGLES, nullptr, 1, sizeof(DrawArraysIndirectCommand));
		vao_->unbind();
	}

	/**
	 * Switch between rendering temperature or debug color
	 */
	void GalaxyRenderer::SwitchRenderMode()
	{
		render_mode_debug = !render_mode_debug;
	}

	/**
	 * Add size to current sun size
	 * @param size_to_add size for addition
	 */
	void GalaxyRenderer::SetSunSize(float size_to_add)
	{
		sun_radius_ = std::max(1.0f, sun_radius_ + size_to_add);
		reload_sun_model_ = true;
	}

	/**
	 * Generate sun model for rendering 
	 */
	void GalaxyRenderer::LoadSunModel()
	{
		// Prepare planet mesh
		std::vector<glm::vec3> sphereData;
		CreateSphereGeometry(sphereData);
		vbo_ = std::make_shared<ge::gl::Buffer>(gl_->getFunctionTable(), sizeof(glm::vec3) * sphereData.size(), sphereData.data());
		vao_->addAttrib(vbo_, 0, 3, GL_FLOAT);
	}

	/**
	 * Create sphere model for drawing sun set
	 * @param vertexData container for created sphere vertices
	 */
	void GalaxyRenderer::CreateSphereGeometry(std::vector<glm::vec3>& vertexData) 
	{
		const int vertiesPerFace = 6;
		sphere_vertices_ = sun_tess_x_* sun_tess_y_*vertiesPerFace;
		const float radius = sun_radius_;
		for (int y = 0; y < sun_tess_y_; ++y) {
			for (int x = 0; x < sun_tess_x_; ++x) {
				for (int k = 0; k < vertiesPerFace; ++k) {
					const int xOffset[] = { 0,1,0,0,1,1 };
					const int yOffset[] = { 0,0,1,1,0,1 };
					const float u = static_cast<float>(x + xOffset[k]) / sun_tess_x_;
					const float v = static_cast<float>(y + yOffset[k]) / sun_tess_y_;
					const float xAngle = -u * glm::two_pi<float>();
					const float yAngle = v * glm::pi<float>();
					const glm::vec3 normal(cos(xAngle)*sin(yAngle), -cos(yAngle), sin(xAngle)*sin(yAngle));
					glm::vec3 position = normal*radius;
					vertexData.emplace_back(position);
				}
			}
		}
	}
#endif
}
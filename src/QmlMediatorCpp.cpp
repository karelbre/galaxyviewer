#include <QmlMediatorCpp.h>

namespace galaxy_viewer
{
	/**
	 * Initialize component
	 */
	void QmlMediatorCpp::Init(QQmlContext* context, AppController* app_controller)
	{
		context_ = context;
		app_controller_ = app_controller;

		InitProperties();
		NotifyPropertyChange();
		CreateConnections();
		LoadDefaultSettings();
	}

	/**
	 * Connect mediator to app frontend
	 */
	void QmlMediatorCpp::LoadQmlRoot(QQuickWindow* window)
	{
		window_ = window;

		connect(window, SIGNAL(switchToMainMenu()), this, SLOT(SwitchedToMainMenu()));
		connect(window, SIGNAL(saveGalaxy(QString)), this, SLOT(SaveGalaxy(QString)));
		connect(window, SIGNAL(loadGalaxy(QString, bool)), this, SLOT(LoadGalaxy(QString, bool)));
		connect(window, SIGNAL(saveSettings(QString)), this, SLOT(SaveSettings(QString)));
		connect(window, SIGNAL(loadSettings(QString)), this, SLOT(LoadSettings(QString)));
		connect(window, SIGNAL(loadDefaultSettings()), this, SLOT(LoadDefaultSettings()));
		connect(window, SIGNAL(generate()), this, SLOT(Generate()));
		connect(window, SIGNAL(saveImage()), this, SLOT(SaveImage()));
		connect(window, SIGNAL(changePaintShape(int)), this, SLOT(ChangePaintShape(int)));
		connect(window, SIGNAL(changeChannelState(int, bool)), this, SLOT(ChangeChannelState(int, bool)));
		connect(window, SIGNAL(paintToImage(int, int, int, int, int, int, int, bool)), this, SLOT(PaintToImage(int, int, int, int, int, int, int, bool)));
		connect(window, SIGNAL(changePauseState(bool)), this, SLOT(ChangePauseState(bool)));
	}

	/**
	 * Create mediator connections with app backend
	 */
	void QmlMediatorCpp::CreateConnections() const
	{
		connect(app_controller_,
			SIGNAL(SettingsFromFileLoaded(galaxy_generator::SphereData, galaxy_generator::ClusterData,
				galaxy_generator::GridData, galaxy_generator::SpiralData, galaxy_generator::HeatmapGenData)),
			this, 
			SLOT(LoadSettings(galaxy_generator::SphereData, galaxy_generator::ClusterData, galaxy_generator::GridData, 
				galaxy_generator::SpiralData, galaxy_generator::HeatmapGenData)));

		connect(app_controller_, SIGNAL(CameraPositionChanged(int, int, int)), this, SLOT(ChangeCameraPosition(int, int, int)));
		connect(app_controller_, SIGNAL(GalaxyGenerated(int, int, std::string)), this, SLOT(GalaxyGenerated(int, int, std::string)));
		connect(app_controller_, SIGNAL(TriggerPauseMenu()), this, SLOT(TriggerPauseMenu()));
	}

	/**
	 * Create properties for frontend
	 */
	void QmlMediatorCpp::InitProperties()
	{
		sphere_data_.insert("size", 0);
		sphere_data_.insert("density_deviation", 0);
		sphere_data_.insert("density_mean", 0);
		sphere_data_.insert("deviation_x", 0);
		sphere_data_.insert("deviation_y", 0);
		sphere_data_.insert("deviation_z", 0);

		cluster_data_.insert("basis", 0);
		cluster_data_.insert("count_deviation", 0);
		cluster_data_.insert("count_mean", 0);
		cluster_data_.insert("deviation_x", 0);
		cluster_data_.insert("deviation_y", 0);
		cluster_data_.insert("deviation_z", 0);

		grid_data_.insert("size", 0);
		grid_data_.insert("spacing", 0);

		spiral_data_.insert("size", 0);
		spiral_data_.insert("spacing", 0);
		spiral_data_.insert("minimum_arms", 0);
		spiral_data_.insert("maximum_arms", 0);
		spiral_data_.insert("cluster_count_deviation", 0);
		spiral_data_.insert("cluster_center_deviation", 0);
		spiral_data_.insert("min_arm_cluster_scale", 0);
		spiral_data_.insert("max_arm_cluster_scale", 0);
		spiral_data_.insert("arm_cluster_scale_deviation", 0);
		spiral_data_.insert("swirl", 0);
		spiral_data_.insert("center_cluster_scale", 0);
		spiral_data_.insert("center_cluster_density_mean", 0);
		spiral_data_.insert("center_cluster_density_deviation", 0);
		spiral_data_.insert("center_cluster_size_deviation", 0);
		spiral_data_.insert("center_cluster_position_deviation", 0);
		spiral_data_.insert("center_cluster_count_deviation", 0);
		spiral_data_.insert("center_cluster_count_mean", 0);
		spiral_data_.insert("central_void_size_mean", 0);
		spiral_data_.insert("central_void_size_deviation", 0);

		heatmap_data_.insert("size_x", 0);
		heatmap_data_.insert("size_y", 0);
		heatmap_data_.insert("size_z", 0);
		heatmap_data_.insert("temp_range_min", 0);
		heatmap_data_.insert("temp_range_max", 0);
		heatmap_data_.insert("height_deviation", 0);
		heatmap_data_.insert("temp_deviation", 0);
		heatmap_data_.insert("minimal_distance", 0);

		galaxy_info_.insert("camera_position", "(0,0,0)");
		galaxy_info_.insert("heatmap_filename", "unkwown");
		galaxy_info_.insert("suns_amount", 0);
		galaxy_info_.insert("generate_method", 0);
		galaxy_info_.insert("generate_shape", 0);
		galaxy_info_.insert("basis_amount", 0);
	}

	/**
	 * Update properties
	 */
	void QmlMediatorCpp::NotifyPropertyChange()
	{
		context_->setContextProperty("sphere_data", &sphere_data_);
		context_->setContextProperty("cluster_data", &cluster_data_);
		context_->setContextProperty("grid_data", &grid_data_);
		context_->setContextProperty("spiral_data", &spiral_data_);
		context_->setContextProperty("heatmap_data", &heatmap_data_);
		context_->setContextProperty("galaxy_info", &galaxy_info_);
	}

	/**
	 * Get properties of selected shape
	 */
	std::shared_ptr<galaxy_generator::BaseShapeData> QmlMediatorCpp::GetShapeData(galaxy_generator::galaxy_type type)
	{
		switch(type)
		{
			case galaxy_generator::SPHERE:
			{
				std::shared_ptr<galaxy_generator::SphereData> data = std::make_shared<galaxy_generator::SphereData>
				(
					glm::vec2(),
					sphere_data_["size"].toFloat(),
					sphere_data_["density_deviation"].toFloat(),
					sphere_data_["density_mean"].toFloat(),
					sphere_data_["deviation_x"].toFloat(),
					sphere_data_["deviation_y"].toFloat(),
					sphere_data_["deviation_z"].toFloat()
				);
				return data;
			}

			case galaxy_generator::CLUSTER:
			{
				std::shared_ptr<galaxy_generator::ClusterData> data = std::make_shared<galaxy_generator::ClusterData>
				(
					glm::vec2(),
					nullptr,
					cluster_data_["basis"].toInt(),
					cluster_data_["count_deviation"].toFloat(),
					cluster_data_["count_mean"].toFloat(),
					cluster_data_["deviation_x"].toFloat(),
					cluster_data_["deviation_y"].toFloat(),
					cluster_data_["deviation_z"].toFloat()
				);
				return data;
			}

			case galaxy_generator::GRID:
			{
				std::shared_ptr<galaxy_generator::GridData> data = std::make_shared<galaxy_generator::GridData>
				(
					glm::vec2(),
					grid_data_["size"].toFloat(),
					grid_data_["spacing"].toFloat()
				);
				return data;
			}

			case galaxy_generator::SPIRAL:
			{
				std::shared_ptr<galaxy_generator::SpiralData> data = std::make_shared<galaxy_generator::SpiralData>
				(
					glm::vec2(),
					spiral_data_["size"].toFloat(),
					spiral_data_["spacing"].toFloat(),
					spiral_data_["minimum_arms"].toInt(),
					spiral_data_["maximum_arms"].toInt(),
					spiral_data_["cluster_count_deviation"].toFloat(),
					spiral_data_["cluster_center_deviation"].toFloat(),
					spiral_data_["min_arm_cluster_scale"].toFloat(),
					spiral_data_["max_arm_cluster_scale"].toFloat(),
					spiral_data_["arm_cluster_scale_deviation"].toFloat(),
					spiral_data_["swirl"].toFloat(),
					spiral_data_["center_cluster_scale"].toFloat(),
					spiral_data_["center_cluster_density_mean"].toFloat(),
					spiral_data_["center_cluster_density_deviation"].toFloat(),
					spiral_data_["center_cluster_size_deviation"].toFloat(),
					spiral_data_["center_cluster_position_deviation"].toFloat(),
					spiral_data_["center_cluster_count_deviation"].toFloat(),
					spiral_data_["center_cluster_count_mean"].toFloat(),
					spiral_data_["central_void_size_mean"].toFloat(),
					spiral_data_["central_void_size_deviation"].toFloat()
				);
				return data;
			}

			default:
			{
				qDebug() << "Not registered type!";
				return nullptr;
			}
		}
	}

// QML to C++
#if 1
	/**
	 * Load properties from default settings file
	 */
	void QmlMediatorCpp::LoadDefaultSettings() const
	{
		app_controller_->LoadSettingsFromFile(QString(GalaxyViewer_RESOURCES) + "configs/DefaultSettings.galaxysettings");
	}

	/**
	 * Load properties to frontend
	 * @param sphere_data sphere properties
	 * @param cluster_data cluster properties
	 * @param grid_data grid properties
	 * @param spiral_data spiral properties
	 * @param heatmap_data heatmap properties
	 */
	void QmlMediatorCpp::LoadSettings(galaxy_generator::SphereData sphere_data, galaxy_generator::ClusterData cluster_data, 
		galaxy_generator::GridData grid_data, galaxy_generator::SpiralData spiral_data, galaxy_generator::HeatmapGenData heatmap_data)
	{
		sphere_data_["size"] = sphere_data.size_;
		sphere_data_["density_deviation"] = sphere_data.density_deviation_;
		sphere_data_["density_mean"] = sphere_data.density_mean_;
		sphere_data_["deviation_x"] = sphere_data.deviation_x_;
		sphere_data_["deviation_y"] = sphere_data.deviation_y_;
		sphere_data_["deviation_z"] = sphere_data.deviation_z_;

		cluster_data_["basis"] = cluster_data.basis_type_;
		cluster_data_["count_deviation"] = cluster_data.count_deviation_;
		cluster_data_["count_mean"] = cluster_data.count_mean_;
		cluster_data_["deviation_x"] = cluster_data.deviation_x_;
		cluster_data_["deviation_y"] = cluster_data.deviation_y_;
		cluster_data_["deviation_z"] = cluster_data.deviation_z_;

		grid_data_["size"] = grid_data.size_;
		grid_data_["spacing"] = grid_data.spacing_;

		spiral_data_["size"] = spiral_data.size_;
		spiral_data_["spacing"] = spiral_data.spacing_;
		spiral_data_["minimum_arms"] = spiral_data.minimum_arms_;
		spiral_data_["maximum_arms"] = spiral_data.maximum_arms_;
		spiral_data_["cluster_count_deviation"] = spiral_data.cluster_count_deviation_;
		spiral_data_["cluster_center_deviation"] = spiral_data.cluster_center_deviation_;
		spiral_data_["min_arm_cluster_scale"] = spiral_data.min_arm_cluster_scale_;
		spiral_data_["max_arm_cluster_scale"] = spiral_data.max_arm_cluster_scale_;
		spiral_data_["arm_cluster_scale_deviation"] = spiral_data.arm_cluster_scale_deviation_;
		spiral_data_["swirl"] = spiral_data.swirl_;
		spiral_data_["center_cluster_scale"] = spiral_data.center_cluster_scale_;
		spiral_data_["center_cluster_density_mean"] = spiral_data.center_cluster_density_mean_;
		spiral_data_["center_cluster_density_deviation"] = spiral_data.center_cluster_density_deviation_;
		spiral_data_["center_cluster_size_deviation"] = spiral_data.center_cluster_size_deviation_;
		spiral_data_["center_cluster_position_deviation"] = spiral_data.center_cluster_position_deviation_;
		spiral_data_["center_cluster_count_deviation"] = spiral_data.center_cluster_count_deviation_;
		spiral_data_["center_cluster_count_mean"] = spiral_data.center_cluster_count_mean_;
		spiral_data_["central_void_size_mean"] = spiral_data.central_void_size_mean_;
		spiral_data_["central_void_size_deviation"] = spiral_data.central_void_size_deviation_;

		heatmap_data_["size_x"] = heatmap_data.galaxy_size_.x;
		heatmap_data_["size_y"] = heatmap_data.galaxy_size_.y;
		heatmap_data_["size_z"] = heatmap_data.galaxy_size_.z;
		heatmap_data_["temp_range_min"] = heatmap_data.temp_range_.x;
		heatmap_data_["temp_range_max"] = heatmap_data.temp_range_.y;
		heatmap_data_["height_deviation"] = heatmap_data.height_deviation_;
		heatmap_data_["temp_deviation"] = heatmap_data.temp_deviation_;
		heatmap_data_["minimal_distance"] = heatmap_data.minimal_distance_;
		
		NotifyPropertyChange();
	}

	/**
	 * Load properties from file
	 * @param filepath source file
	 */
	void QmlMediatorCpp::LoadSettings(QString filepath) const
	{
		app_controller_->LoadSettingsFromFile(filepath.mid(8));
	}

	/**
	 * Save properties to file
	 */
	void QmlMediatorCpp::SaveSettings(QString filepath)
	{
		const galaxy_generator::SphereData sphere_data
		(
			glm::vec2(),
			sphere_data_["size"].toFloat(),
			sphere_data_["density_deviation"].toFloat(),
			sphere_data_["density_mean"].toFloat(),
			sphere_data_["deviation_x"].toFloat(),
			sphere_data_["deviation_y"].toFloat(),
			sphere_data_["deviation_z"].toFloat()
		);

		const galaxy_generator::ClusterData cluster_data
		(
			glm::vec2(),
			nullptr,
			cluster_data_["basis"].toInt(),
			cluster_data_["count_deviation"].toFloat(),
			cluster_data_["count_mean"].toFloat(),
			cluster_data_["deviation_x"].toFloat(),
			cluster_data_["deviation_y"].toFloat(),
			cluster_data_["deviation_z"].toFloat()
		);

		const galaxy_generator::GridData grid_data
		(
			glm::vec2(),
			grid_data_["size"].toFloat(),
			grid_data_["spacing"].toFloat()
		);

		const galaxy_generator::SpiralData spiral_data
		(
			glm::vec2(),
			spiral_data_["size"].toFloat(),
			spiral_data_["spacing"].toFloat(),
			spiral_data_["minimum_arms"].toInt(),
			spiral_data_["maximum_arms"].toInt(),
			spiral_data_["cluster_count_deviation"].toFloat(),
			spiral_data_["cluster_center_deviation"].toFloat(),
			spiral_data_["min_arm_cluster_scale"].toFloat(),
			spiral_data_["max_arm_cluster_scale"].toFloat(),
			spiral_data_["arm_cluster_scale_deviation"].toFloat(),
			spiral_data_["swirl"].toFloat(),
			spiral_data_["center_cluster_scale"].toFloat(),
			spiral_data_["center_cluster_density_mean"].toFloat(),
			spiral_data_["center_cluster_density_deviation"].toFloat(),
			spiral_data_["center_cluster_size_deviation"].toFloat(),
			spiral_data_["center_cluster_position_deviation"].toFloat(),
			spiral_data_["center_cluster_count_deviation"].toFloat(),
			spiral_data_["center_cluster_count_mean"].toFloat(),
			spiral_data_["central_void_size_mean"].toFloat(),
			spiral_data_["central_void_size_deviation"].toFloat()
		);

		const galaxy_generator::HeatmapGenData heatmap_data
		{
			nullptr,
			glm::vec3(
				heatmap_data_["size_x"].toFloat(),
				heatmap_data_["size_y"].toFloat(),
				heatmap_data_["size_z"].toFloat()),
			glm::vec2(
				heatmap_data_["temp_range_min"].toFloat(),
				heatmap_data_["temp_range_max"].toFloat()),
			heatmap_data_["height_deviation"].toFloat(),
			heatmap_data_["temp_deviation"].toFloat(),
			heatmap_data_["minimal_distance"].toInt()
		};

		app_controller_->SaveSettingsToFile(filepath.mid(8), sphere_data, cluster_data, grid_data, spiral_data, heatmap_data);
	}

	/**
	 * Generate galaxy
	 */
	void QmlMediatorCpp::Generate()
	{
		if (galaxy_info_["generate_method"] == 1)
		{
			galaxy_generator::HeatmapGenData heatmap_data
			{
				nullptr,
				glm::vec3(
					heatmap_data_["size_x"].toFloat(),
					heatmap_data_["size_y"].toFloat(),
					heatmap_data_["size_z"].toFloat()
				),
				glm::vec2(
					heatmap_data_["temp_range_min"].toFloat(),
					heatmap_data_["temp_range_max"].toFloat()
				),
				heatmap_data_["height_deviation"].toFloat(),
				heatmap_data_["temp_deviation"].toFloat(),
				heatmap_data_["minimal_distance"].toInt()
			};
			// Heatmap method
			app_controller_->Generate(heatmap_data);
		}
		else
		{
			galaxy_generator::galaxy_type type;
			// Shape method
			switch(galaxy_info_["generate_shape"].toInt())
			{
				case 0:
				{
					type = galaxy_generator::SPHERE;
					break;
				}

				case 1:
				{
					type = galaxy_generator::GRID;
					break;
				}

				case 2:
				{
					type = galaxy_generator::CLUSTER;
					break;
				}

				case 3:
				{
					type = galaxy_generator::SPIRAL;
					break;
				}

				default:
				{
					qDebug() << "Not valid shape id. Not generating!";
					return;
				}
			}

			std::shared_ptr<galaxy_generator::BaseShapeData> data = GetShapeData(type);
			if (type == galaxy_generator::CLUSTER)
			{
				const std::shared_ptr<galaxy_generator::ClusterData> cluster_data = std::static_pointer_cast<galaxy_generator::ClusterData>(data);
				galaxy_generator::BaseShape* basis;
				switch (cluster_data_["basis"].toInt())
				{
					case 0:
					{
						const std::shared_ptr<galaxy_generator::SphereData> sphere_data =
							std::static_pointer_cast<galaxy_generator::SphereData>(GetShapeData(galaxy_generator::SPHERE));
						basis = new galaxy_generator::Sphere(sphere_data, nullptr, nullptr);
						break;
					}

					case 1:
					{
						const std::shared_ptr<galaxy_generator::GridData> grid_data =
							std::static_pointer_cast<galaxy_generator::GridData>(GetShapeData(galaxy_generator::GRID));
						basis = new galaxy_generator::Grid(grid_data, nullptr, nullptr);
						break;
					}

					case 2:
					{
						const std::shared_ptr<galaxy_generator::SpiralData> spiral_data =
							std::static_pointer_cast<galaxy_generator::SpiralData>(GetShapeData(galaxy_generator::SPIRAL));
						basis = new galaxy_generator::Spiral(spiral_data, nullptr, nullptr);
						break;
					}

					default:
					{
						qDebug() << "Cluster basis type is not set";
						const std::shared_ptr<galaxy_generator::SphereData> sphere_data =
							std::static_pointer_cast<galaxy_generator::SphereData>(GetShapeData(galaxy_generator::SPHERE));
						basis = new galaxy_generator::Sphere(sphere_data, nullptr, nullptr);
					}
				}

				cluster_data->basis_ = basis;
			}

			app_controller_->Generate(type, data);
		}
	}

	/**
	 * Pause drawing galaxy
	 */
	void QmlMediatorCpp::StopRender() const
	{
		app_controller_->StopRender();
	}

	/**
	 * Paint brush pattern to heatmap in editor
	 * @param x brush x position
	 * @param y brush y position
	 * @param size brush size
	 * @param red brush red channel
	 * @param green brush green channel
	 * @param blue brush blue channel
	 * @param alpha brush alpha channel
	 * @param is_preview is paint temporary or permanent?
	 */
	void QmlMediatorCpp::PaintToImage(int x, int y, int size, int red, int green, int blue, int alpha, bool is_preview) const
	{
		app_controller_->PaintToImage(glm::vec2(x, y), size, glm::vec4(red, green, blue, alpha), is_preview);
	}

	/**
	 * Turn visibility of heatmap channel
	 * @param channel channel id (0-3)
	 * @param state visibility
	 */
	void QmlMediatorCpp::ChangeChannelState(int channel, bool state) const
	{
		app_controller_->SetImageChannel(channel, state);
	}
	
	/**
	 * Change brush shape 
	 * @param shape_type (0 = circle, 1 = rectangle)
	 */
	void QmlMediatorCpp::ChangePaintShape(int shape_type) const
	{
		app_controller_->ChangePainterShape(shape_type);
	}

	/**
	 * (Un)pause application backend
	 */
	void QmlMediatorCpp::ChangePauseState(bool state) const
	{
		app_controller_->SetPauseState(state);
	}

	/**
	 * Application triggered to switch from space view to main menu
	 */
	void QmlMediatorCpp::SwitchedToMainMenu() const
	{
		app_controller_->StopRender();
	}

	/**
	 * Save heatmap image to disk
	 */
	void QmlMediatorCpp::SaveImage() const
	{
		app_controller_->SaveImage();
	}

	/**
	 * Save galaxy to file
	 * @param filepath destination file
	 */
	void QmlMediatorCpp::SaveGalaxy(QString filepath) const
	{
		app_controller_->SaveGalaxyToFile(filepath.mid(8));
	}

	/**
	 * Load galaxy from file
	 * @param filepath source file
	 * @param start_render enable galaxy drawing?
	 */
	void QmlMediatorCpp::LoadGalaxy(QString filepath, bool start_render) const
	{
		app_controller_->LoadGalaxyFromFile(filepath.mid(8));
		if (start_render)
		{
			app_controller_->StartRender();
		}
	}

#endif

// C++ to QML 
#if 1
	/**
	 * Reaction on galaxy creation. Forwarding to frontend
	 * @param suns_amount amount of galaxy's sun list
	 * @param basis_amount amount of clusters (if set cluster shape for generate, otherwise = 0)
	 * @param heatmap_filename used heatmap file (if set heatmap for generate)
	 */
	void QmlMediatorCpp::GalaxyGenerated(int suns_amount, int basis_amount, std::string heatmap_filename)
	{
		galaxy_info_["basis_amount"] = basis_amount;
		galaxy_info_["suns_amount"] = suns_amount;
		galaxy_info_["camera_position"] = "(0,0,0)";
		galaxy_info_["heatmap_filename"] = heatmap_filename.c_str();

		NotifyPropertyChange();
		QMetaObject::invokeMethod(window_, "onGalaxyGenerated");
	}

	/**
	 * Reaction on camera position changed. Forwarding to frontend
	 * @param x camera x position
	 * @param y camera y position
	 * @param z camera z position
	 */
	void QmlMediatorCpp::ChangeCameraPosition(int x, int y, int z)
	{
		galaxy_info_["camera_position"] = QString("(%1,%2,%3)").arg(x).arg(y).arg(z);
		NotifyPropertyChange();
	}

	/**
	 * Reaction on QQuickWindow resized. Forwarding to frontend
	 */
	void QmlMediatorCpp::Resize() const
	{
		QMetaObject::invokeMethod(window_, "onResize");
	}

	/**
	 * Reaction on triggered pause menu in space view. Forwarding to frontend
	 */
	void QmlMediatorCpp::TriggerPauseMenu() const
	{
		QMetaObject::invokeMethod(window_, "onTriggeredPauseMenu");
	}
#endif
}
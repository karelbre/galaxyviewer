#include <AppController.h>

#include <QFile>
#include <QJsonObject>
#include <QJsonArray>
#include <QJsonDocument>
#include <QPoint>
#include <QCursor>

namespace galaxy_viewer
{
	/**
	 * Constructor
	 */
	AppController::AppController()
	{
		app_paused_ = true;
		lock_cursor_ = false;
		heatmap_preview_ = nullptr;
		heatmap_painter_ = nullptr;
		input_controller_ = nullptr;
		window_ = nullptr;
		renderer_ = nullptr;
	}

	/**
	 * Destructor
	 */
	AppController::~AppController()
	{
		if (input_controller_) delete input_controller_;
	}

	/**
	 * Initialize component. Need to call before anything else!
	 * @param window reference to QQuickWindow main object
	 * @param image_painter object provides editing heatmap image
	 * @param heatmap object provides heatmap for generate
	 */
	void AppController::Init(
		QQuickWindow* window,
		HeatmapImageProvider* image_painter, 
		HeatmapPreviewProvider* heatmap)
	{
		window_ = window;
		renderer_ = new GalaxyRenderer(window);
		input_controller_ = new InputController();
		heatmap_painter_ = image_painter;
		heatmap_preview_ = heatmap;
		window_->installEventFilter(input_controller_);

		SetWindowCenter();

		connect(renderer_, SIGNAL(CameraPositionChanged(int, int, int)), this, SLOT(ChangeCameraPosition(int, int, int)));
		connect(this, SIGNAL(UpdateRenderer()), window_, SLOT(update()));
		connect(input_controller_, SIGNAL(OnKeyPressed(int)), this, SLOT(OnKeyPressed(int)));
		connect(input_controller_, SIGNAL(OnKeyReleased(int)), this, SLOT(OnKeyReleased(int)));
		connect(input_controller_, SIGNAL(OnMouseMove(int, int)), this, SLOT(OnMouseMove(int, int)));
		connect(input_controller_, SIGNAL(OnMouseLeave()), this, SLOT(OnMouseLeave()));
		connect(input_controller_, SIGNAL(OnResize()), this, SLOT(OnResize()));
		connect(input_controller_, SIGNAL(OnFocusIn()), this, SLOT(OnFocusIn()));
		connect(input_controller_, SIGNAL(CursorNeedToBeCentered()), this, SLOT(SetCursorToCenter()));
	}

	/**
	 * Save galaxy sun list to file
	 * @param filepath destination for galaxy save file
	 */
	void AppController::SaveGalaxyToFile(QString filepath) const
	{
		QJsonObject galaxy_info;
		
		QJsonObject extra_info;

		QJsonObject sun_list_data;
		QJsonArray sun_list_position;
		QJsonArray sun_list_temperature;
		QJsonArray sun_list_temp_range;

		const std::shared_ptr<std::vector<galaxy_generator::SunListData>> sun_list_vec = renderer_->GetSunListVecData();
		const int max = static_cast<int>(sun_list_vec->size());
		for (int i = 0; i < max; ++i)
		{
			const galaxy_generator::SunListData data = sun_list_vec->at(i);
			sun_list_position.append(QJsonArray{ data.position_.x, data.position_.y, data.position_.z });
			sun_list_temperature.append(QJsonValue{ data.temperature_ });
		}

		const glm::vec2 temp_range = renderer_->GetTemperatureRangeData();
		sun_list_temp_range.append(QJsonValue{ temp_range.x });
		sun_list_temp_range.append(QJsonValue{ temp_range.y });

		sun_list_data.insert("sun_list_position", sun_list_position);
		sun_list_data.insert("sun_list_temperature", sun_list_temperature);
		sun_list_data.insert("sun_list_temp_range", sun_list_temp_range);

		const glm::vec3 camera_position = renderer_->GetCameraPosition();
		const glm::vec3 camera_rotation = renderer_->GetCameraRotation();
		const glm::vec2 aabb_min = renderer_->galaxy_aabb_min_;
		const glm::vec2 aabb_max = renderer_->galaxy_aabb_max_;
		extra_info.insert("camera_position", QJsonArray{ camera_position.x, camera_position.y, camera_position.z });
		extra_info.insert("camera_rotation", QJsonArray{ camera_rotation.x, camera_rotation.y, camera_rotation.z });
		extra_info.insert("galaxy_aabb_min", QJsonArray{ aabb_min.x, aabb_min.y });
		extra_info.insert("galaxy_aabb_max", QJsonArray{ aabb_max.x, aabb_max.y });
		extra_info.insert("save_version", 1);

		galaxy_info.insert("extra_info", extra_info);
		galaxy_info.insert("sun_list_data", sun_list_data);

		QJsonDocument d{ galaxy_info };
		QString val = d.toJson();

		QFile file;
		file.setFileName(filepath);
		file.open(QIODevice::WriteOnly | QIODevice::Text);
		file.write(val.toUtf8());
		file.close();
	}

	/**
	 * Load galaxy sun list from file
	 * @param filepath source of galaxy save file
	 */
	void AppController::LoadGalaxyFromFile(QString filepath) const
	{
		QFile file;
		file.setFileName(filepath);
		file.open(QIODevice::ReadOnly | QIODevice::Text);
		QString val = file.readAll();
		file.close();

		QJsonDocument d = QJsonDocument::fromJson(val.toUtf8());
		QJsonObject galaxy_info = d.object();

		QJsonObject extra_info = galaxy_info["extra_info"].toObject();
		QJsonArray camera_position_json = extra_info["camera_position"].toArray();
		QJsonArray camera_rotation_json = extra_info["camera_rotation"].toArray();
		
		QJsonObject sun_list_data = galaxy_info["sun_list_data"].toObject();
		QJsonArray sun_list_position = sun_list_data["sun_list_position"].toArray();
		QJsonArray sun_list_temperature = sun_list_data["sun_list_temperature"].toArray();
		QJsonArray sun_list_temp_range = sun_list_data["sun_list_temp_range"].toArray();

		if (sun_list_position.size() != sun_list_temperature.size())
		{
			qDebug() << "Galaxy save is corrupted! Different size of records!";
			return;
		}

		std::shared_ptr<std::vector<galaxy_generator::SunListData>> sun_list = std::make_shared<std::vector<galaxy_generator::SunListData>>();
		const int max = sun_list_position.size();
		for (int i = 0; i < max; ++i)
		{
			QJsonArray position = sun_list_position.at(i).toArray();
			QJsonValue temperature = sun_list_temperature.at(i);

			galaxy_generator::SunListData data
			{ 
				glm::vec3(position[0].toDouble(), position[1].toDouble(), position[2].toDouble()), 
				static_cast<float>(temperature.toDouble()) 
			};
			sun_list->emplace_back(data);
		}

		const glm::vec2 temp_range(sun_list_temp_range[0].toDouble(), sun_list_temp_range[1].toDouble());
		const glm::vec3 camera_position = glm::vec3(
			camera_position_json[0].toDouble(), 
			camera_position_json[1].toDouble(), 
			camera_position_json[2].toDouble());
		const glm::vec3 camera_rotation = glm::vec3(
			camera_rotation_json[0].toDouble(),
			camera_rotation_json[1].toDouble(),
			camera_rotation_json[2].toDouble());

		renderer_->SetSunListData(sun_list, temp_range, camera_position, camera_rotation);
	}

	/**
	* Save app settings to file
	* @param filepath destination for settings file
	* @param sphere_data sphere settings
	* @param cluster_data cluster settings
	* @param grid_data grid settings
	* @param spiral_data spiral settings
	* @param heatmap_data heatmap settings
	*/
	void AppController::SaveSettingsToFile(
		QString filepath,
		galaxy_generator::SphereData sphere_data,
		galaxy_generator::ClusterData cluster_data,
		galaxy_generator::GridData grid_data,
		galaxy_generator::SpiralData spiral_data,
		galaxy_generator::HeatmapGenData heatmap_data)
	{
		const QJsonObject sphere_data_json
		{
			{ "size", sphere_data.size_ },
			{ "density_deviation", sphere_data.density_deviation_ },
			{ "density_mean", sphere_data.density_mean_ },
			{ "deviation_x", sphere_data.deviation_x_ },
			{ "deviation_y", sphere_data.deviation_y_ },
			{ "deviation_z", sphere_data.deviation_z_ },
		};

		const QJsonObject cluster_data_json
		{
			{ "basis_type", cluster_data.basis_type_ },
			{ "count_deviation", cluster_data.count_deviation_ },
			{ "count_mean", cluster_data.count_mean_ },
			{ "deviation_x", cluster_data.deviation_x_ },
			{ "deviation_y", cluster_data.deviation_y_ },
			{ "deviation_z", cluster_data.deviation_z_ },
		};

		const QJsonObject grid_data_json
		{
			{ "size", grid_data.size_ },
			{ "spacing", grid_data.spacing_ }
		};

		const QJsonObject spiral_data_json
		{
			{ "size", spiral_data.size_ },
			{ "spacing", spiral_data.spacing_ },
			{ "minimum_arms", spiral_data.minimum_arms_ },
			{ "maximum_arms", spiral_data.maximum_arms_ },
			{ "cluster_count_deviation", spiral_data.cluster_count_deviation_ },
			{ "cluster_center_deviation", spiral_data.cluster_center_deviation_ },
			{ "min_arm_cluster_scale", spiral_data.min_arm_cluster_scale_ },
			{ "max_arm_cluster_scale", spiral_data.max_arm_cluster_scale_ },
			{ "arm_cluster_scale_deviation", spiral_data.arm_cluster_scale_deviation_ },
			{ "swirl", spiral_data.swirl_ },
			{ "center_cluster_scale", spiral_data.center_cluster_scale_ },
			{ "center_cluster_density_mean", spiral_data.center_cluster_density_mean_ },
			{ "center_cluster_density_deviation", spiral_data.center_cluster_density_deviation_ },
			{ "center_cluster_size_deviation", spiral_data.center_cluster_size_deviation_ },
			{ "center_cluster_position_deviation", spiral_data.center_cluster_position_deviation_ },
			{ "center_cluster_count_deviation", spiral_data.center_cluster_count_deviation_ },
			{ "center_cluster_count_mean", spiral_data.center_cluster_count_mean_ },
			{ "central_void_size_mean", spiral_data.central_void_size_mean_ },
			{ "central_void_size_deviation", spiral_data.central_void_size_deviation_ }
		};

		const QJsonObject heatmap_data_json
		{
			{ "size_x", heatmap_data.galaxy_size_.x },
			{ "size_y", heatmap_data.galaxy_size_.y },
			{ "size_z", heatmap_data.galaxy_size_.z },
			{ "temp_range_min", heatmap_data.temp_range_.x },
			{ "temp_range_max", heatmap_data.temp_range_.y },
			{ "temp_deviation", heatmap_data.temp_deviation_ },
			{ "height_deviation", heatmap_data.height_deviation_ },
			{ "minimal_distance", heatmap_data.minimal_distance_ },
		};

		const QJsonObject all_data
		{
			{ "sphere_data", sphere_data_json },
			{ "cluster_data", cluster_data_json },
			{ "grid_data", grid_data_json },
			{ "spiral_data", spiral_data_json },
			{ "heatmap_data", heatmap_data_json }
		};

		QJsonDocument d{ all_data };
		QString val = d.toJson();

		QFile file;
		file.setFileName(filepath);
		file.open(QIODevice::WriteOnly | QIODevice::Text);
		file.write(val.toUtf8());
		file.close();
	}

	/**
	 * Load app settings from file
	 * @param filepath source of settings file
	 */
	void AppController::LoadSettingsFromFile(QString filepath)
	{
		QFile file;
		file.setFileName(filepath);
		file.open(QIODevice::ReadOnly | QIODevice::Text);
		QString val = file.readAll();
		file.close();

		QJsonDocument d = QJsonDocument::fromJson(val.toUtf8());
		QJsonObject all_data = d.object();
		QJsonObject heatmap_data_json = all_data.value("heatmap_data").toObject();
		QJsonObject sphere_data_json = all_data.value("sphere_data").toObject();
		QJsonObject cluster_data_json = all_data.value("cluster_data").toObject();
		QJsonObject grid_data_json = all_data.value("grid_data").toObject();
		QJsonObject spiral_data_json = all_data.value("spiral_data").toObject();

		const galaxy_generator::SphereData sphere_data(
			glm::vec2(),
			sphere_data_json.value("size").toDouble(),
			sphere_data_json.value("density_deviation").toDouble(),
			sphere_data_json.value("density_mean").toDouble(),
			sphere_data_json.value("deviation_x").toDouble(),
			sphere_data_json.value("deviation_y").toDouble(),
			sphere_data_json.value("deviation_z").toDouble()
		);

		const galaxy_generator::ClusterData cluster_data(
			glm::vec2(),
			nullptr,
			cluster_data_json.value("basis_type").toInt(),
			cluster_data_json.value("count_deviation").toDouble(),
			cluster_data_json.value("count_mean").toDouble(),
			cluster_data_json.value("deviation_x").toDouble(),
			cluster_data_json.value("deviation_y").toDouble(),
			cluster_data_json.value("deviation_z").toDouble()
		);

		const galaxy_generator::GridData grid_data(
			glm::vec2(),
			grid_data_json.value("size").toDouble(),
			grid_data_json.value("spacing").toDouble()
		);

		const galaxy_generator::SpiralData spiral_data(
			glm::vec2(),
			spiral_data_json.value("size").toDouble(),
			spiral_data_json.value("spacing").toDouble(),
			spiral_data_json.value("minimum_arms").toInt(),
			spiral_data_json.value("maximum_arms").toInt(),
			spiral_data_json.value("cluster_count_deviation").toDouble(),
			spiral_data_json.value("cluster_center_deviation").toDouble(),
			spiral_data_json.value("min_arm_cluster_scale").toDouble(),
			spiral_data_json.value("max_arm_cluster_scale").toDouble(),
			spiral_data_json.value("arm_cluster_scale_deviation").toDouble(),
			spiral_data_json.value("swirl").toDouble(),
			spiral_data_json.value("center_cluster_scale").toDouble(),
			spiral_data_json.value("center_cluster_density_mean").toDouble(),
			spiral_data_json.value("center_cluster_density_deviation").toDouble(),
			spiral_data_json.value("center_cluster_size_deviation").toDouble(),
			spiral_data_json.value("center_cluster_position_deviation").toDouble(),
			spiral_data_json.value("center_cluster_count_deviation").toDouble(),
			spiral_data_json.value("center_cluster_count_mean").toDouble(),
			spiral_data_json.value("central_void_size_mean").toDouble(),
			spiral_data_json.value("central_void_size_deviation").toDouble()
		);

		const galaxy_generator::HeatmapGenData heatmap_data
		{
			nullptr,
			glm::vec3(
				heatmap_data_json.value("size_x").toDouble(),
				heatmap_data_json.value("size_y").toDouble(),
				heatmap_data_json.value("size_z").toDouble()),
			glm::vec2(
				heatmap_data_json.value("temp_range_min").toDouble(),
				heatmap_data_json.value("temp_range_max").toDouble()),
			static_cast<float>(heatmap_data_json.value("height_deviation").toDouble()),
			static_cast<float>(heatmap_data_json.value("temp_deviation").toDouble()),
			heatmap_data_json.value("minimal_distance").toInt(),
		};

		SettingsFromFileLoaded(sphere_data, cluster_data, grid_data, spiral_data, heatmap_data);
	}

	/**
	 * Generate galaxy of selected shape
	 * @param type galaxy shape
	 * @param data shape generator settings
	 */
	void AppController::Generate(galaxy_generator::galaxy_type type, std::shared_ptr<galaxy_generator::BaseShapeData> data)
	{
		data->temp_range_ = glm::vec2(1000, 10000);

		galaxy_generator::GalaxyGen galaxy_gen;
		galaxy_gen.SetGeneratorSettings(data);
		const std::shared_ptr<std::vector<galaxy_generator::SunListData>> sun_list = galaxy_gen.Generate(type);
		
		float basis_amount = 0;
		const std::shared_ptr<galaxy_generator::BaseExtraInfo> extra_info = galaxy_gen.GetExtraInfo(type);
		if (extra_info != nullptr)
		{
			if (type == galaxy_generator::CLUSTER)
			{
				const std::shared_ptr<galaxy_generator::ClusterExtraInfo> clusterInfo = std::static_pointer_cast<galaxy_generator::ClusterExtraInfo>(extra_info);
				basis_amount = clusterInfo->basis_amount_;
			}
		}

		LoadGeneratedGalaxy(sun_list, data->temp_range_, extra_info->galaxy_size_min_, extra_info->galaxy_size_max_, basis_amount);
	}

	/**
	 * Generate galaxy by heatmap
	 * @param heatmap_data heatmap generator settings
	 */
	void AppController::Generate(galaxy_generator::HeatmapGenData heatmap_data)
	{
		QSize size = heatmap_preview_->GetSize();
		galaxy_generator::Heatmap map;
		map.SetSourceImage(size.width(), size.height(), heatmap_preview_->GetPixels());
		heatmap_data.heatmap_ = std::make_shared<galaxy_generator::Heatmap>(map);
			
		galaxy_generator::GalaxyGen galaxy_gen;
		const std::shared_ptr<std::vector<galaxy_generator::SunListData>> sun_list_vec = galaxy_gen.Generate(heatmap_data);
		const std::shared_ptr<galaxy_generator::BaseExtraInfo> extra_info = galaxy_gen.GetExtraInfo(galaxy_generator::UNKNOWN);
		
		LoadGeneratedGalaxy(sun_list_vec, heatmap_data.temp_range_, extra_info->galaxy_size_min_, extra_info->galaxy_size_max_);
	}

	/**
	 * Start galaxy drawing, reset camera position and cursor lock
	 */
	void AppController::StartRender()
	{
		//renderer_->ResetCameraPosition();
		renderer_->SetPause(false);
		app_paused_ = false;
		SetCursorState(false);
	}

	/**
	 * Pause galaxy drawing
	 */
	void AppController::StopRender() const
	{
		renderer_->SetPause(true);
	}

	/**
	 * (Un)pause app controller
	 */
	void AppController::SetPauseState(bool state)
	{
		app_paused_ = state;
		renderer_->ResetMovement();
		SetCursorState(state);
	}

	/**
	 * Paint pattern to heatmap preview
	 * @param xy brush position
	 * @param size brush size
	 * @param color brush color
	 * @param is_preview is paint only for preview or permanent
	 */
	void AppController::PaintToImage(glm::vec2 xy, int size, glm::vec4 color, bool is_preview) const
	{
		heatmap_painter_->SetColor(xy.x, xy.y, size, QColor::fromRgb(color.r, color.g, color.b, color.a), is_preview);
	}

	/**
	 * Change state of heatmap texture channel
	 * @param channel id of color channel (0-3 rgba)
	 * @param state channel state (on/off)
	 */
	void AppController::SetImageChannel(int channel, bool state) const
	{
		heatmap_painter_->SetChannel(static_cast<PainterBase::channel_type>(channel), state);
	}

	/**
	 * Change brush pattern
	 * @param shape_type brush id (0 = circle, 1 = rectangle)
	 */
	void AppController::ChangePainterShape(int shape_type) const
	{
		heatmap_painter_->ChangePainter(static_cast<HeatmapImageProvider::shape_type>(shape_type));
	}

	/**
	 * Save preview image to file (accept changes)
	 */
	void AppController::SaveImage() const
	{
		heatmap_painter_->SaveAndReset();
	}

// From GUI slots
#if 1
	/**
	 * Set cursor to (in)visibility
	 * @param state visible = true
	 */
	void AppController::SetCursorState(bool state) const
	{
		QCursor cursor = window_->cursor();
		cursor.setShape(state ? Qt::ArrowCursor : Qt::BlankCursor); // for release
		window_->setCursor(cursor);
	}

	/**
	 * Set cursor position to the center of window
	 */
	void AppController::SetCursorToCenter() const
	{
		if (app_paused_)
		{
			return;
		}

		if (!lock_cursor_)
		{
			QCursor::setPos(window_->mapToGlobal(window_center_));
		}
	}
#endif

// To GUI slots
#if 1
	/**
	 * Inform GUI about camera position changed
	 * @param x camera x position
	 * @param y camera y position
	 * @param z camera z position
	 */
	void AppController::ChangeCameraPosition(int x, int y, int z)
	{
		emit CameraPositionChanged(x, y, z);
	}
#endif

// Input management
#if 1
	/**
	 * Reaction for any key pressed
	 */
	void AppController::OnKeyPressed(int key)
	{
		if (key == Qt::Key_Escape)
		{
			emit TriggerPauseMenu();
		}
		
		if (app_paused_)
		{
			return;
		}

		switch (key)
		{
			case Qt::Key_R:
				renderer_->ResetCameraPosition();
				break;

			case Qt::Key_M:
				renderer_->SwitchRenderMode();
				break;

			case Qt::Key_B:
				renderer_->SetSunSize(1);
				break;

			case Qt::Key_N:
				renderer_->SetSunSize(-1);
				break;

			default:
				SetMoveState(key, true);
				break;
		}
	}

	/**
	 * Reaction for any key released
	 */
	void AppController::OnKeyReleased(int key)
	{
		if (app_paused_)
		{
			return;
		}

		SetMoveState(key, false);
	}

	/**
	 * Reaction for mouse position changed. Camera rotation
	 */
	void AppController::OnMouseMove(int x, int y) const
	{
		if (app_paused_)
		{
			return;
		}

		if (!lock_cursor_)
		{
			renderer_->SetRotation(x, y);
		}
	}

	/**
	 * Reaction for mouse leave window. Set cursor position back to the center
	 */
	void AppController::OnMouseLeave() const
	{
		if (app_paused_)
		{
			return;
		}

		if (!lock_cursor_)
		{
			QCursor::setPos(window_->mapToGlobal(window_center_));
		}
	}

	/**
	 * Reaction for window gets focus. Lock cursor
	 */
	void AppController::OnFocusIn()
	{
		if (app_paused_)
		{
			return;
		}

		QCursor cursor = window_->cursor();
		cursor.setShape(Qt::BlankCursor);
		window_->setCursor(cursor);
		lock_cursor_ = false;
	}

	/**
	 * Reaction for window resize. Recompute GUI and renderer values
	 */
	void AppController::OnResize() 
	{
		SetWindowCenter();
		renderer_->OnResize();
	}

#endif 

// Private methods
#if 1
	/**
	 * Set generated sun list to render and starts draw
	 * @param sun_list_vec generated sun list
	 * @param temp_range range of generated sun list temperature
	 * @param cluster_basis_amount amount of generated cluster (set only if cluster was selected)
	 */
	void AppController::LoadGeneratedGalaxy(const std::shared_ptr<std::vector<galaxy_generator::SunListData>> sun_list_vec,
		glm::vec2 temp_range, glm::vec3 galaxy_aabb_min, glm::vec3 galaxy_aabb_max, int cluster_basis_amount)
	{
		renderer_->galaxy_aabb_min_ = galaxy_aabb_min;
		renderer_->galaxy_aabb_max_ = galaxy_aabb_max;
		const glm::vec3 camera_position = ComputeCameraPosition(galaxy_aabb_min, galaxy_aabb_max);
		const glm::vec3 camera_rotation = ComputeCameraRotation(camera_position, (galaxy_aabb_max - galaxy_aabb_min) / 2.0f);
		renderer_->SetSunListData(sun_list_vec, temp_range, camera_position, camera_rotation);
		StartRender();
		SetCursorState(false);
		emit GalaxyGenerated(static_cast<int>(sun_list_vec->size()), cluster_basis_amount, heatmap_preview_->GetFilename().toStdString());
	}

	/**
	 * Set input key state
	 * @param key selected key
	 * @param state current state of key
	 */
	void AppController::SetMoveState(int key, bool state)
	{
		switch (key)
		{
		case Qt::Key_W:
			renderer_->SetMoving(GalaxyRenderer::move_dir::FORWARD, state);
			break;

		case Qt::Key_A:
			renderer_->SetMoving(GalaxyRenderer::move_dir::LEFT, state);
			break;

		case Qt::Key_S:
			renderer_->SetMoving(GalaxyRenderer::move_dir::BACK, state);
			break;

		case Qt::Key_D:
			renderer_->SetMoving(GalaxyRenderer::move_dir::RIGHT, state);
			break;

		case Qt::Key_C:
			renderer_->SetMoving(GalaxyRenderer::move_dir::DOWN, state);
			break;

		case Qt::Key_Space:
			renderer_->SetMoving(GalaxyRenderer::move_dir::UP, state);
			break;

		case Qt::Key_Shift:
			renderer_->SetMoving(GalaxyRenderer::move_dir::SHIFT, state);
			break;

		case Qt::Key_Slash:
			renderer_->SetSensitivity(GalaxyRenderer::MOVEMENT, true);
			break;

		case Qt::Key_Asterisk:
			renderer_->SetSensitivity(GalaxyRenderer::MOVEMENT, false);
			break;

		case Qt::Key_Minus:
			renderer_->SetSensitivity(GalaxyRenderer::ROTATION, false);
			break;

		case Qt::Key_Plus:
			renderer_->SetSensitivity(GalaxyRenderer::ROTATION, true);
			break;

		case Qt::Key_Control:
			{
				lock_cursor_ = state;
				QCursor cursor = window_->cursor();
				cursor.setShape(lock_cursor_ ? Qt::ArrowCursor : Qt::BlankCursor);
				window_->setCursor(cursor);
			}
			break;

		default:
			break;
		}
	}

	/**
	 * Set center point of window for correct cursor lock
	 */
	void AppController::SetWindowCenter()
	{
		window_center_ = QPoint(window_->width() / 2.0f, window_->height() / 2.0f);
		input_controller_->Init(window_center_);
	}

	/**
	 * Compute camera position to look over whole galaxy
	 * @param aabb_min minimum of bounding box of generated galaxy
	 * @param aabb_max maximum of bounding box of generated galaxy
	 */
	glm::vec3 AppController::ComputeCameraPosition(glm::vec3 aabb_min, glm::vec3 aabb_max) const
	{
		const float dx = std::abs(aabb_min.x - aabb_max.x);
		const float dy = std::abs(aabb_min.y - aabb_max.y);
		const float dz = std::abs(aabb_min.z - aabb_max.z);
		const float fov_part = std::tan(45 * 3.1415 / 180 / 2);

		glm::vec3 pos{
			dx / 2 / fov_part,
			dy / 2 / fov_part,
			dz / 2 / fov_part };

		return pos;
	}

	/**
	 * Compute camera rotation to center
	 * @param camera_position position of camera in space
	 * @param galaxy_center center of galaxy bounding box
	 */
	glm::vec3 AppController::ComputeCameraRotation(glm::vec3 camera_position, glm::vec3 galaxy_center) const
	{
		glm::mat4 res = glm::lookAt(camera_position, galaxy_center, glm::vec3(0, 0, 1));
		const float angle_x = std::atan2(res[1][2], res[2][2]);
		const float angle_y = std::atan2(-res[0][2], std::sqrt(res[1][2] * res[1][2]) + (res[2][2] * res[2][2]));
		glm::vec3 rotation_euler{ angle_x, angle_y, 0 };

		return rotation_euler;
	}
#endif
}
#include <ShapePainters.h>
#include <QDebug>

namespace galaxy_viewer
{
	/**
	 * Paint brush color to image
	 * @param image_origin final image
	 * @param image_modify preview image
	 * @param pos cursor position
	 * @param color brush color
	 * @param channel_states channels visibility
	 */
	void PainterRect::Paint(QImage& image_origin, QImage& image_modify, glm::vec2 pos, float size, QColor color, bool* channel_states)
	{
		const QColor channelColor = QColor::fromRgb(
			channel_states[RED] ? color.red() : 0,
			channel_states[GREEN] ? color.green() : 0,
			channel_states[BLUE] ? color.blue() : 0,
			channel_states[ALPHA] ? color.alpha() : 0);

		const glm::ivec2 min = glm::vec2(
			std::max(static_cast<int>(pos.x - size), 0),
			std::max(static_cast<int>(pos.y - size), 0));

		const glm::ivec2 max = glm::vec2(
			std::min(static_cast<int>(pos.x + size), image_modify.size().width()-1),
			std::min(static_cast<int>(pos.y + size), image_modify.size().height()-1));
		
		for (int j = min.y; j <= max.y; ++j)
		{
			for (int i = min.x; i <= max.x; ++i)
			{
				QColor originalColor = image_origin.pixelColor(i, j);
				image_origin.setPixelColor(i, j, 
					QColor(
						channel_states[RED] ? channelColor.red() : originalColor.red(),
						channel_states[GREEN] ? channelColor.green() : originalColor.green(),
						channel_states[BLUE] ? channelColor.blue() : originalColor.blue(),
						channel_states[ALPHA] ? channelColor.alpha() : originalColor.alpha()));
				image_modify.setPixelColor(i, j, channelColor);
			}
		}
	}

	/**
	 * Paint brush color to image as preview
	 * @param image preview image
	 * @param pos cursor position
	 * @param color brush color
	 * @param channel_states channels visibility
	 */
	void PainterRect::PaintPreview(QImage& image, glm::vec2 pos, float size, QColor color, bool* channel_states)
	{
		const QColor channelColor = QColor::fromRgb(
			channel_states[RED] ? color.red() : 0,
			channel_states[GREEN] ? color.green() : 0,
			channel_states[BLUE] ? color.blue() : 0,
			channel_states[ALPHA] ? color.alpha() : 0);

		const glm::ivec2 min = glm::vec2(
			std::max(static_cast<int>(pos.x - size), 0),
			std::max(static_cast<int>(pos.y - size), 0));

		const glm::ivec2 max = glm::vec2(
			std::min(static_cast<int>(pos.x + size), image.size().width() - 1),
			std::min(static_cast<int>(pos.y + size), image.size().height() - 1));

		for (int j = min.y; j <= max.y; ++j)
		{
			for (int i = min.x; i <= max.x; ++i)
			{
				image.setPixelColor(i, j, channelColor);
			}
		}
	}

	/**
	 * Remove preview paint from image
	 * @param image_origin final image
	 * @param image_modify preview image
	 * @@param pos cursor position
	 * @param channel_states channels visibility
	 */
	void PainterRect::RemovePreview(QImage& image_origin, QImage& image_modify, glm::vec2 pos, float size, bool* channel_states)
	{
		const glm::ivec2 min = glm::vec2(
			std::max(static_cast<int>(pos.x - size), 0),
			std::max(static_cast<int>(pos.y - size), 0));

		const glm::ivec2 max = glm::vec2(
			std::min(static_cast<int>(pos.x + size), image_modify.size().width() - 1),
			std::min(static_cast<int>(pos.y + size), image_modify.size().height() - 1));

		for (int j = min.y; j <= max.y; ++j)
		{
			for (int i = min.x; i <= max.x; ++i)
			{
				QColor pixel_color = image_origin.pixelColor(i, j);
				pixel_color = QColor::fromRgb(
					channel_states[RED] ? pixel_color.red() : 0,
					channel_states[GREEN] ? pixel_color.green() : 0,
					channel_states[BLUE] ? pixel_color.blue() : 0,
					channel_states[ALPHA] ? pixel_color.alpha() : 0);
				image_modify.setPixelColor(i, j, pixel_color);
			}
		}
	}

	/**
	 * Paint brush color to image
	 * @param image_origin final image
	 * @param image_modify preview image
	 * @param pos cursor position
	 * @param size brush size
	 * @param color brush color
	 * @param channel_states channels visibility
	 */
	void PainterCircle::Paint(QImage& image_origin, QImage& image_modify, glm::vec2 pos, float size, QColor color, bool* channel_states)
	{
		const QColor channelColor = QColor::fromRgb(
			channel_states[RED] ? color.red() : 0,
			channel_states[GREEN] ? color.green() : 0,
			channel_states[BLUE] ? color.blue() : 0,
			channel_states[ALPHA] ? color.alpha() : 0);

		const glm::ivec2 min = glm::vec2(
			std::max(static_cast<int>(pos.x - size), 0),
			std::max(static_cast<int>(pos.y - size), 0));

		const glm::ivec2 max = glm::vec2(
			std::min(static_cast<int>(pos.x + size), image_modify.size().width() - 1),
			std::min(static_cast<int>(pos.y + size), image_modify.size().height() - 1));

		const float radius = size / 2.0f;
		for (int j = min.y; j <= max.y; ++j)
		{
			for (int i = min.x; i <= max.x; ++i)
			{
				const float distance_to_centre = sqrt((i - pos.x)*(i - pos.x) + (j - pos.y)*(j - pos.y));
				if (distance_to_centre < radius + 0.25f)
				{
					QColor originalColor = image_origin.pixelColor(i, j);
					image_origin.setPixelColor(i, j,
						QColor(
							channel_states[RED] ? channelColor.red() : originalColor.red(),
							channel_states[GREEN] ? channelColor.green() : originalColor.green(),
							channel_states[BLUE] ? channelColor.blue() : originalColor.blue(),
							channel_states[ALPHA] ? channelColor.alpha() : originalColor.alpha()));
					image_modify.setPixelColor(i, j, channelColor);
				}
			}
		}
	}

	/**
	 * Paint brush color to image as preview
	 * @param image preview image
	 * @param pos cursor position
	 * @param size brush size
	 * @param color brush color
	 * @param channel_states channels visibility
	 */
	void PainterCircle::PaintPreview(QImage& image, glm::vec2 pos, float size, QColor color, bool* channel_states)
	{
		const QColor channelColor = QColor::fromRgb(
			channel_states[RED] ? color.red() : 0,
			channel_states[GREEN] ? color.green() : 0,
			channel_states[BLUE] ? color.blue() : 0,
			channel_states[ALPHA] ? color.alpha() : 0);

		const glm::ivec2 min = glm::vec2(
			std::max(static_cast<int>(pos.x - size), 0),
			std::max(static_cast<int>(pos.y - size), 0));

		const glm::ivec2 max = glm::vec2(
			std::min(static_cast<int>(pos.x + size), image.size().width() - 1),
			std::min(static_cast<int>(pos.y + size), image.size().height() - 1));

		const float radius = size / 2.0f;
		for (int j = min.y; j <= max.y; ++j)
		{
			for (int i = min.x; i <= max.x; ++i)
			{
				const float distance_to_centre = sqrt((i - pos.x)*(i - pos.x) + (j - pos.y)*(j - pos.y));
				if (distance_to_centre < radius + 0.25f)
				{
					image.setPixelColor(i, j, channelColor);
				}
			}
		}
	}

	/**
	 * Paint brush color to image
	 * @param image_origin final image
	 * @param image_modify preview image
	 * @param pos cursor position
	 * @param size brush size
	 * @param channel_states channels visibility
	 */
	void PainterCircle::RemovePreview(QImage& image_origin, QImage& image_modify, glm::vec2 pos, float size, bool* channel_states)
	{
		const glm::ivec2 min = glm::vec2(
			std::max(static_cast<int>(pos.x - size), 0),
			std::max(static_cast<int>(pos.y - size), 0));

		const glm::ivec2 max = glm::vec2(
			std::min(static_cast<int>(pos.x + size), image_modify.size().width() - 1),
			std::min(static_cast<int>(pos.y + size), image_modify.size().height() - 1));

		const float radius = size / 2.0f;
		for (int j = min.y; j <= max.y; ++j)
		{
			for (int i = min.x; i <= max.x; ++i)
			{
				const float distance_to_centre = sqrt((i - pos.x)*(i - pos.x) + (j - pos.y)*(j - pos.y));
				if (distance_to_centre < radius + 0.25f)
				{
					QColor pixel_color = image_origin.pixelColor(i, j);
					pixel_color = QColor::fromRgb(
						channel_states[RED] ? pixel_color.red() : 0,
						channel_states[GREEN] ? pixel_color.green() : 0,
						channel_states[BLUE] ? pixel_color.blue() : 0,
						channel_states[ALPHA] ? pixel_color.alpha() : 0);
					image_modify.setPixelColor(i, j, pixel_color);
				}
			}
		}
	}
}
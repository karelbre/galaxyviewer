// AUTHOR: Karel Brezina
// DATE: 16.7.2017

#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QTimer>

#include <AppController.h>
#include <QmlMediatorCpp.h>
#include <HeatmapImageProvider.h>
#include <HeatmapPreviewProvider.h>

int main(int argc, char *argv[])
{
	// Register new QML object type
	qmlRegisterType<galaxy_viewer::QmlMediatorCpp>("GalaxyViewer.QmlMediatorCpp", 1, 0, "QmlMediatorCpp");

	// Create main app classes
	galaxy_viewer::AppController appController;
	galaxy_viewer::QmlMediatorCpp mediator;
	galaxy_viewer::HeatmapImageProvider* imagePainter = new galaxy_viewer::HeatmapImageProvider();
	galaxy_viewer::HeatmapPreviewProvider* imagePreview = new galaxy_viewer::HeatmapPreviewProvider();
	
	// Create Qt classes
	QGuiApplication app(argc, argv);
	QQmlApplicationEngine engine;
	QQmlContext* context = engine.rootContext();
	
	const QIcon icon{QString(GalaxyViewer_RESOURCES) + "textures/AppIcon.ico"};
	app.setWindowIcon(icon);

	// For release uncomment without console
#if WIN32
	if (!((argc > 1) && (std::string(argv[1]) == "-showcmd")))
	{
		FreeConsole();
	}
#endif*/

	// Prepare mediator
	mediator.Init(context, &appController);
	context->setContextProperty("mediator", &mediator);
	// Register image providers
	engine.addImageProvider(QStringLiteral("heatmap"), imagePainter);
	engine.addImageProvider(QStringLiteral("preview"), imagePreview);
	// Load QML main window
	engine.load(QUrl("qrc:/qml/MainWindow.qml"));
	
	// Get QML main window
	QObject *topLevel = engine.rootObjects().value(0);
	QQuickWindow *window = qobject_cast<QQuickWindow*>(topLevel);
	mediator.LoadQmlRoot(window);
	//window->showFullScreen(); 
	
	// App main object
	appController.Init(window, imagePainter, imagePreview);

	// Run Forest run!
	return app.exec();
}

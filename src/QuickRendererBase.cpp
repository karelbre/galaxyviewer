#include <QOpenGLContext>
#include <QuickRendererBase.h>
#include <QDebug>

/**
 * If parent is a QQuickWindow or subclas then calls setParentWindow().
 * @see SetParentWindow()
 */
QuickRendererBase::QuickRendererBase(QObject* parent)
   : QObject(parent)
   , qqw_(nullptr)
{
   QQuickWindow* qqw = dynamic_cast<QQuickWindow*>(parent);
   if (nullptr != qqw)
   {
      SetParentWindow(qqw);      
   }
}

/**
 * Reimplement to make your rendering before Qt. Opengl context should be active here.
 */
void QuickRendererBase::OnBeforeRendering()
{
   if (qqw_ == nullptr)
   {
      qDebug() << "Set parent QQuickWindow with SetParentWindow().";
      return;
   }

   // when you change the opengl state you must call this at the end.
   qqw_->resetOpenGLState();
}

/**
* Reimplement to make your rendering after Qt. Opengl context should be active here.
*/
void QuickRendererBase::OnAfterRendering()
{
	if (qqw_ == nullptr)
	{
		qDebug() << "Set parent QQuickWindow with setParentWindow().";
		return;
	}

	// when you change the opengl state you must call this at the end.
	qqw_->resetOpenGLState();
}

/**
 * Reimplement to make you gl initialization here. Context context was just created.
 * It is not current and \b needs to be released at the end of the function.
 */
void QuickRendererBase::OnOGLContextCreated(QOpenGLContext*)
{ }

/**
 * Make connection with QQuickWindow rendering related signals.
 * \li OnBeforeRendering()
 * \li OnAfterRendering()
 * \li openglContextCreated(QOpenGLContext *)
 * \li sceneGraphInvalidated()
 * \li frameSwapped()
 * If called with the same window that it is already connected to then nothing will happen. If called with null
 * the signals get disconnected. When called with another window then signals get reconnected.
 */
void QuickRendererBase::SetParentWindow(QQuickWindow * qqw)
{
   if (qqw_ != nullptr)
   {
		if (qqw_ == qqw)
		{
			return;
		}

		disconnect(qqw, SIGNAL(beforeRendering()), this, SLOT(OnBeforeRendering()));
		disconnect(qqw, SIGNAL(afterRendering()), this, SLOT(OnAfterRendering()));
      disconnect(qqw, SIGNAL(openglContextCreated(QOpenGLContext*)), this, SLOT(OnOGLContextCreated(QOpenGLContext*)));
      disconnect(qqw, SIGNAL(sceneGraphInvalidated()), this, SLOT(OnSceneGraphInvalidated()));
      disconnect(qqw, SIGNAL(frameSwapped()), this, SLOT(OnFrameSwapped()));
   }

   qqw_ = qqw;
   if(qqw != nullptr)
   {
		connect(qqw, SIGNAL(beforeRendering()), this, SLOT(OnBeforeRendering()), Qt::DirectConnection);
		connect(qqw, SIGNAL(afterRendering()), this, SLOT(OnAfterRendering()), Qt::DirectConnection);
      connect(qqw, SIGNAL(openglContextCreated(QOpenGLContext *)), this, SLOT(OnOGLContextCreated(QOpenGLContext *)), Qt::DirectConnection);
		connect(qqw, SIGNAL(sceneGraphInvalidated()), this, SLOT(OnSceneGraphInvalidated()), Qt::DirectConnection);
      connect(qqw, SIGNAL(frameSwapped()), this, SLOT(OnFrameSwapped()), Qt::DirectConnection);
      qqw_->setClearBeforeRendering(false);
   }
}

/**
 * Reimplement to react to opengl context invalidation. Possible cleanup
 */
void QuickRendererBase::OnSceneGraphInvalidated()
{ }

/**
 * Reimplement to react on frame swap.
 */
void QuickRendererBase::OnFrameSwapped()
{ }
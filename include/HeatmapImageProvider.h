// AUTHOR: Karel B�ezina
// DATE: 2.2.2018

#pragma once

#include <memory>
#include <QQuickImageProvider>

#include <glm/vec2.hpp>

#include <ShapePainters.h>

namespace galaxy_viewer
{
	/**
	 * Class provides editing heatmap
	 */
	class HeatmapImageProvider : public QQuickImageProvider
	{
	public:
		/**
		 * Brush types
		 */
		enum shape_type {CIRCLE, RECTANGLE};

		HeatmapImageProvider();
		QImage requestImage(const QString& id, QSize* size, const QSize& requestedSize) override;
		void SetChannel(PainterBase::channel_type channel, bool state);
		void SetColor(int x, int y, int size, QColor color, bool is_preview);
		void ChangePainter(shape_type type);
		void SaveAndReset();
		bool IsImageLoaded() const;

	private:
		bool channel_state_[4] = { true, true, true, true };
		
		QString filename_;
		QImage heatmap_modified_;
		QImage heatmap_all_channels_;
		QImage heatmap_original_;

		glm::vec2 paint_pos_preview_;
		float paint_width_preview_;
		bool was_last_preview_;

		shape_type painter_type_;
		std::shared_ptr<PainterBase> painter_;
	};
}

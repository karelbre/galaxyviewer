#pragma once

#include <glm/vec2.hpp>
#include <QColor>
#include <QImage>

namespace galaxy_viewer
{
	/**
	 * Paint brush base class
	 */
	class PainterBase
	{
	public:
		enum channel_type { RED, GREEN, BLUE, ALPHA };

		virtual ~PainterBase() = default;
		virtual void Paint(QImage& image_origin, QImage& image_modify, glm::vec2 pos, float size, QColor color, bool* channel_states) = 0;
		virtual void PaintPreview(QImage& image, glm::vec2 pos, float size, QColor color, bool* channel_states) = 0;
		virtual void RemovePreview(QImage& image_origin, QImage& image_modify, glm::vec2 pos, float size, bool* channel_states) = 0;
	};

	/**
	 * Rectangle brush painter
	 */
	class PainterRect : public PainterBase
	{
	public:
		void Paint(QImage& image_origin, QImage& image_modify, glm::vec2 pos, float size, QColor color, bool* channel_states) override;
		void PaintPreview(QImage& image, glm::vec2 pos, float size, QColor color, bool* channel_states) override;
		void RemovePreview(QImage& image_origin, QImage& image_modify, glm::vec2 pos, float size, bool* channel_states) override;
	};

	/**
	 * Sphere brush painter
	 */
	class PainterCircle : public PainterBase
	{
	public:
		void Paint(QImage& image_origin, QImage& image_modify, glm::vec2 pos, float size, QColor color, bool* channel_states) override;
		void PaintPreview(QImage& image, glm::vec2 pos, float size, QColor color, bool* channel_states) override;
		void RemovePreview(QImage& image_origin, QImage& image_modify, glm::vec2 pos, float size, bool* channel_states) override;
	};
}
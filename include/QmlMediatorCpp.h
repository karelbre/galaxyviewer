﻿// AUTHOR: Karel Brezina
// DATE: 19.6.2017

#pragma once

#include <QObject>
#include <QString>
#include <QQmlPropertyMap>
#include <QQmlContext>

#include <GalaxyGen.h>
#include <AppController.h>

namespace galaxy_viewer
{
	/*
	 *	Connecting QML frontend and C++ backend 
	 */
	class QmlMediatorCpp : public QObject
	{
		Q_OBJECT

	public:
		void Init(QQmlContext* context, AppController* app_controller);
		void LoadQmlRoot(QQuickWindow* window);

	private slots:
		// QML to C++ way
		void Generate();
		void StopRender() const;
		void SaveSettings(QString filepath);
		void LoadDefaultSettings() const;
		void LoadSettings(QString filepath) const;
		void PaintToImage(int x, int y, int size, int red, int green, int blue, int alpha, bool is_preview) const;
		void ChangeChannelState(int channel, bool state) const;
		void ChangePaintShape(int shape_type) const;
		void ChangePauseState(bool state) const;
		void SwitchedToMainMenu() const;
		void SaveImage() const;
		void SaveGalaxy(QString filepath) const;
		void LoadGalaxy(QString filepath, bool start_render) const;
		// C++ to QML way
		void LoadSettings(galaxy_generator::SphereData sphere_data, galaxy_generator::ClusterData cluster_data,
			galaxy_generator::GridData grid_data, galaxy_generator::SpiralData spiral_data, galaxy_generator::HeatmapGenData heatmap_data);
		void Resize() const;
		void GalaxyGenerated(int suns_amount, int basis_amount, std::string heatmap_filename);
		void ChangeCameraPosition(int x, int y, int z);
		void TriggerPauseMenu() const;

	private:
		void CreateConnections() const;
		void InitProperties();
		void NotifyPropertyChange();
		std::shared_ptr<galaxy_generator::BaseShapeData> GetShapeData(galaxy_generator::galaxy_type type);

		QQmlContext* context_;
		QQuickWindow* window_;
		AppController* app_controller_;

		QQmlPropertyMap sphere_data_;
		QQmlPropertyMap cluster_data_;
		QQmlPropertyMap grid_data_;
		QQmlPropertyMap spiral_data_;
		QQmlPropertyMap heatmap_data_;
		QQmlPropertyMap galaxy_info_;
	};
}

// AUTHOR: Karel B�ezina
// DATE: 2.2.2018

#pragma once

#include <QQuickImageProvider>

#include <ShapePainters.h>

namespace galaxy_viewer
{
	/**
	 * Class provides heatmap texture for preview and generator
	 */
	class HeatmapPreviewProvider : public QQuickImageProvider
	{
	public:
		HeatmapPreviewProvider();
		QImage requestImage(const QString& id, QSize*, const QSize&) override;
		glm::vec4** GetPixels() const;
		QSize GetSize() const;
		QString GetFilename() const;
		bool IsImageLoaded() const;

	private:
		QString filename_;
		QImage heatmap_;
	};
}
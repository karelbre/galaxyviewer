// AUTHOR: Karel Brezina
// DATE: 7.11.2017

#pragma once

#include <QObject>
#include <QString>

#include <GalaxyRenderer.h>
#include <InputController.h>
#include <HeatmapPreviewProvider.h>
#include <HeatmapImageProvider.h>

#include <GalaxyGen.h>

namespace galaxy_viewer
{
	/**
	 * Class controls application 
	 */
	class AppController : public QObject
	{
		Q_OBJECT
	public:
		AppController();
		~AppController();

		void Init(QQuickWindow* window, HeatmapImageProvider* heatmap_painter, HeatmapPreviewProvider* heatmap_preview);
		void SaveGalaxyToFile(QString filepath) const;
		void LoadGalaxyFromFile(QString filepath) const;
		static void SaveSettingsToFile(QString filepath, galaxy_generator::SphereData sphere_data, galaxy_generator::ClusterData cluster_data,
			galaxy_generator::GridData grid_data, galaxy_generator::SpiralData spiral_data, galaxy_generator::HeatmapGenData heatmap_data);
		void LoadSettingsFromFile(QString filepath);
		void Generate(galaxy_generator::galaxy_type type, std::shared_ptr<galaxy_generator::BaseShapeData> data);
		void Generate(galaxy_generator::HeatmapGenData heatmap_data);
		void StartRender();
		void StopRender() const;
		void SetPauseState(bool state);
		void PaintToImage(glm::vec2 xy, int size, glm::vec4 color, bool is_preview) const;
		void SetImageChannel(int channel, bool state) const;
		void ChangePainterShape(int shape_type) const;
		void SaveImage() const;

	signals:
		void SettingsFromFileLoaded(galaxy_generator::SphereData sphere_data, galaxy_generator::ClusterData cluster_data,
			galaxy_generator::GridData grid_data, galaxy_generator::SpiralData spiral_data, galaxy_generator::HeatmapGenData heatmap_data);
		void CameraPositionChanged(int x, int y, int z);
		void GalaxyGenerated(int sun_list_amount, int basis_amount, std::string heatmap_filename);
		void UpdateRenderer();
		void TriggerPauseMenu();

	private slots:
		// From GUI
		void SetCursorState(bool state) const;
		void SetCursorToCenter() const;
		// To GUI
		void ChangeCameraPosition(int x, int y, int z);
		// Input management
		void OnKeyPressed(int key);
		void OnKeyReleased(int key);
		void OnMouseMove(int x, int y) const;
		void OnMouseLeave() const;
		void OnFocusIn();
		void OnResize();

	private:
		void LoadGeneratedGalaxy(const std::shared_ptr<std::vector<galaxy_generator::SunListData>> sun_list, glm::vec2 temp_range, 
			glm::vec3 galaxy_aabb_min, glm::vec3 galaxy_aabb_max, int cluster_basis_amount = 0);
		void SetMoveState(int key, bool state);
		void SetWindowCenter();
		glm::vec3 ComputeCameraPosition(glm::vec3 aabb_min, glm::vec3 aabb_max) const;
		glm::vec3 ComputeCameraRotation(glm::vec3 camera_position, glm::vec3 galaxy_center) const;

		bool app_paused_;
		bool lock_cursor_;
		QPoint window_center_;
		
		InputController* input_controller_;
		GalaxyRenderer* renderer_ ;
		HeatmapPreviewProvider* heatmap_preview_;
		HeatmapImageProvider* heatmap_painter_; 
		QQuickWindow* window_;
	};
}

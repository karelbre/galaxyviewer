// AUTHOR: Karel Brezina
// DATE: 18.2.2018

#pragma once

#include <QObject>

#include <GalaxyGen.h>

namespace galaxy_viewer
{
	class SphereData : public QObject
	{
		Q_OBJECT
		Q_PROPERTY(int data_size_ READ SizeRead WRITE SizeWrite NOTIFY SizeChanged)
	public:
		SphereData() { }
		SphereData(const SphereData& data) { data_ = data.data_; }
		//~SphereData();
		SphereData operator=(const SphereData& data) const { return data; }

		int SizeRead() { return data_size_; }
		void SizeWrite(int size) { data_size_ = size; }

	signals:
		void SizeChanged();

	private:
		int data_size_ = 1000;
		galaxy_generator::SphereData data_;
	};

	Q_DECLARE_METATYPE(SphereData)
}

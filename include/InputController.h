// AUTHOR: Karel B�ezina
// DATE: 11.7.2017

#pragma once

#include <QQuickItem>

namespace galaxy_viewer
{
	class InputController : public QObject
	{
		Q_OBJECT

	public:
		InputController();
		void Init(QPoint window_size, float cursor_range = 0.3f);

	signals:
		void OnKeyPressed(int key);
		void OnKeyReleased(int key);
		void OnMouseMove(int dx, int dy);
		void OnMouseEnter();
		void OnMouseLeave();
		void OnFocusIn();
		void OnResize();
		void CursorNeedToBeCentered();

	public slots:
		bool eventFilter(QObject* watched, QEvent* event) override;

	private:
		bool IsCursorNearToCenter(QPoint cursor_pos, QPoint range) const;

		const QPoint centered_range_ = QPoint(20, 20);

		QPoint center_;
		QPoint distance_;
		QPoint last_mouse_pos;
		
		bool waiting_for_cursor_centered_;
		bool is_inited_;
	};
}

// AUTHOR: Karel B�ezina
// DATE: 20.10.2017

#pragma once

#include <memory>

#include <glm/glm.hpp>
#include <geUtil/FreeLookCamera.h>
#include <geUtil/PerspectiveCamera.h>

#include <QuickRendererBase.h>

#include <SunList.h>

namespace ge
{
   namespace gl
   {
      class Program;
		class VertexArray;
		class Buffer;
      class Context;
   }
}

namespace galaxy_viewer
{
	/**
	*  Class drawing generated galaxy
	*/
	class GalaxyRenderer : public QuickRendererBase
	{
		Q_OBJECT

	public:
		/**
		 * Move direction type
		 */
		enum move_dir {FORWARD, BACK, RIGHT, LEFT, UP, DOWN, SHIFT};

		/**
		 * Sensitivity value type
		 */
		enum sensitivity_type {ROTATION, MOVEMENT};
		
		GalaxyRenderer(QObject* parent);
		void SetPause(bool state);
		void Update();
		void SetSunListData(std::shared_ptr<std::vector<galaxy_generator::SunListData>> sun_lists, glm::vec2 temp_range, 
			glm::vec3 camera_position, glm::vec3 camera_rotation);
		void SwitchRenderMode();
		void SetSunSize(float size_to_add);

		// Movement
		void ResetMovement();
		void ResetCameraPosition();
		void SetMoving(move_dir dir, bool state);
		void SetRotation(int x, int y);
		void SetSensitivity(sensitivity_type type, bool is_adding, float value = 1.0f);

		// Callbacks
		void OnResize();
	
		std::shared_ptr<std::vector<galaxy_generator::SunListData>> GetSunListVecData() const { return sun_list_vec_; }
		glm::vec2 GetTemperatureRangeData() const { return temp_range_; }
		glm::vec3 GetCameraPosition() const { return free_look_camera_.getPosition(); }
		glm::vec3 GetCameraRotation() const { return glm::vec3(free_look_camera_.getXAngle(), free_look_camera_.getYAngle(), free_look_camera_.getZAngle()); }

		glm::vec3 galaxy_aabb_min_;
		glm::vec3 galaxy_aabb_max_;

	signals:
		void CameraPositionChanged(int x, int y, int z);

	public slots:
		void OnAfterRendering() override;
		void OnBeforeRendering() override;
		void OnOGLContextCreated(QOpenGLContext* context) override;

	private:
		// Update loop
		void SetupGlState() const;
		void Draw();
		// Suns data management
		void LoadSunModel();
		void CreateSphereGeometry(std::vector<glm::vec3>& vertexData);
		// Movement
		void UpdateCameraRotation();
		void UpdateCameraMove();

		// Data
		std::shared_ptr<std::vector<galaxy_generator::SunListData>> sun_list_vec_;
		glm::vec2 temp_range_;

		bool is_paused_;
		bool is_draw_enabled_;
		bool need_to_reload_;
		bool is_initialized_;
		
		bool move_state_[7] = { false }; 
		float sensitivity_[2] = { 0.01f, 25.0f };
		const float sensitivity_step_vals_[2] = { 0.05f, 1.0f };

		glm::vec2 rotation_delta_;
		float rotation_smooth_factor_;
		int sphere_vertices_;
		bool render_mode_debug;

		// Sun model vars
		bool reload_sun_model_ = false;
		float sun_radius_ = 1.0f;
		int sun_tess_x_ = 20;
		int sun_tess_y_ = 20;

		// Camera vars
		const float camera_move = 0.01f;
		const int acceleration_mul = 5;

		// OGL structures
		std::shared_ptr<ge::gl::Context> gl_;
		std::shared_ptr<ge::gl::Program> shaderProgram_;
		std::shared_ptr<ge::gl::VertexArray> vao_;
		std::shared_ptr<ge::gl::Buffer> vbo_;
		std::shared_ptr<ge::gl::Buffer> indirect_buffer_;
		std::shared_ptr<ge::gl::Buffer> sun_list_buffer_;

		// Cameras
		ge::util::PerspectiveCamera perspective_camera_;
		ge::util::FreeLookCamera free_look_camera_;
	};
}
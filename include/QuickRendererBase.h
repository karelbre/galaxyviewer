#pragma once

#include <QObject>
#include <QQuickWindow>

/**
 * This class provides basic setup for rendering in Qt Quick. It is intended to use with QQuickWindow.
 * For rendering just subclass from this and reimplement virtual functions you need. You should set
 * parent to window you want to render to. The connections are automaticaly made when calling SetParentWindow.
 * Old connections are disconected and new ones are created when the argument is not null.
 */
class QuickRendererBase : public QObject
{
   Q_OBJECT
public:
   QuickRendererBase(QObject* parent = nullptr);
   void SetParentWindow(QQuickWindow* qqw);
	QQuickWindow* GetParentWindow() const { return qqw_; }

public slots:
	virtual void OnBeforeRendering();
	virtual void OnAfterRendering();
   virtual void OnOGLContextCreated(QOpenGLContext* context);
   virtual void OnSceneGraphInvalidated();
   virtual void OnFrameSwapped();

protected:
   QQuickWindow *qqw_;
};
